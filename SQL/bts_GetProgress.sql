IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'bts_GetProgress')
DROP PROCEDURE bts.bts_GetProgress;
GO

CREATE PROCEDURE bts.bts_GetProgress
	@Progress INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

    SELECT
        @Progress = ISNULL(swk_Progress, 0)
    FROM
        bts.swk_SettingsWorker
    WHERE
        swk_ID = 1;
END
GO
