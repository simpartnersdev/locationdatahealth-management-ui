CREATE PROCEDURE bts.bts_GetCurrentStep
	@CurrentStep INT OUT
AS
BEGIN
	SET NOCOUNT ON;

    SELECT
        @CurrentStep = swk_stp_ID
    FROM
        bts.swk_SettingsWorker
    WHERE
        swk_ID = 1;
END
GO
