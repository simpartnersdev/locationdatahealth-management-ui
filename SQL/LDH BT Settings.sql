CREATE SCHEMA bts;
GO

CREATE TABLE bts.stp_WorkerStep
(
    stp_ID          INT PRIMARY KEY,
    stp_Description VARCHAR(100)
);
GO

CREATE TABLE bts.swk_SettingsWorker
(
    swk_ID      INT PRIMARY KEY,
    swk_stp_ID  INT NOT NULL,
    swk_Progress INT
);
GO

ALTER TABLE bts.swk_SettingsWorker WITH CHECK ADD CONSTRAINT FK_swk_SettingsWorker_stp_WorkerStep FOREIGN KEY(swk_stp_ID)
REFERENCES bts.stp_WorkerStep (stp_ID)
GO

INSERT INTO bts.stp_WorkerStep (stp_ID, stp_Description) VALUES (0, 'Idle');
INSERT INTO bts.stp_WorkerStep (stp_ID, stp_Description) VALUES (1, 'Resetting Current ALT Scores');
INSERT INTO bts.stp_WorkerStep (stp_ID, stp_Description) VALUES (2, 'Updating Domain Weights');
INSERT INTO bts.stp_WorkerStep (stp_ID, stp_Description) VALUES (3, 'Calculating New ALT Scores');
GO

INSERT INTO bts.swk_SettingsWorker (swk_ID, swk_stp_ID) VALUES (1, 0);
GO