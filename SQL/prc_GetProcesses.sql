USE ldh;
DROP PROCEDURE `prc_GetProcesses`;
DELIMITER $$
CREATE PROCEDURE `prc_GetProcesses`()
   BEGIN
   
		SELECT
			P.prc_id ID,
            P.prc_date StartDate
		FROM
			ldh.prc_process P
		ORDER BY
			P.prc_date DESC;
         
   END $$
DELIMITER ;


CALL prc_GetProcesses();