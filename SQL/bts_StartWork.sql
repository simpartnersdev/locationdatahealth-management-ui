IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'bts_StartWork')
DROP PROCEDURE bts.bts_StartWork;
GO

CREATE PROCEDURE bts.bts_StartWork
	@CurrentStep INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;

    SELECT
        @CurrentStep = swk_stp_ID
    FROM
        bts.swk_SettingsWorker
    WHERE
        swk_ID = 1;

    IF @CurrentStep = 0 OR @CurrentStep IS NULL
    BEGIN
        SET @CurrentStep = 0; --fix to make sure @currentStep has a value

        UPDATE bts.swk_SettingsWorker
        SET swk_stp_ID = 1, swk_Progress = 0
        WHERE swk_ID = 1;
    END;
END
GO
