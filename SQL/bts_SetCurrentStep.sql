CREATE PROCEDURE bts.bts_SetCurrentStep
	@CurrentStep INT, @Progress INT
AS
BEGIN
	SET NOCOUNT ON;

    UPDATE
        bts.swk_SettingsWorker
    SET
        swk_stp_ID = @CurrentStep,
        swk_Progress = @Progress
    WHERE
        swk_ID = 1;
END
GO
