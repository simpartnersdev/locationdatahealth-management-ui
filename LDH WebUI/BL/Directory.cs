﻿using System;
using System.Collections.Generic;
using System.Configuration;

using LDH.Management.WebUI.Models;
using LDH.Management.WebUI.Models.Enums;
using LDH.Management.WebUI.Models.UI_API;

namespace LDH.Management.WebUI.BL
{
    public class Directory
    {
        public List<Models.Directory> GetDirectories(DirectoryStatus Status)
        {
            return new DAL.Directory().GetDirectories(Status);
        }

        public DirectoryDataResponse GetDirectoriesData(DirectoryStatus Status)
        {
            return new DirectoryDataResponse() { data = new DAL.Directory().GetDirectories(Status) };
        }

        public void GenerateCSVFiles(CSVExportSettings ExportSettings)
        {
            try
            {
                foreach (var directory in ExportSettings.Directories)
                {
                    new DAL.Directory().ExportCSVAccuracyData(directory.DirectoryID, directory.ProcessID, ExportSettings.ExportDate, ExportSettings.ExportToS3);
                }
            }
            catch (Exception ex)
            {
                DAL.Log.AddMessage(string.Format("There were errors while attempting to export a CSV file to {0}. Please check the folder and try again. Error: {1}.\r\nStack Trace: {2}", ConfigurationManager.AppSettings["CSVExportFolder"], ex.Message, ex.StackTrace));
            }
        }

        public ProcessListResponse GetProcessList()
        {
            return new ProcessListResponse { ProcessList = new DAL.Directory().GetProcessList() };
        }
    }
}