﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH.Management.WebUI.BL
{
    public class Process
    {
        public void StartProcess(List<int> DirectoryIDs)
        {
            var LDHProcessID = 0;

            try
            {
                var Directories = new DAL.Directory().GetDirectories(Models.Enums.DirectoryStatus.Active);
                var process = new DAL.Process();

                LDHProcessID = process.CreateProcess();

                foreach (var directory in DirectoryIDs)
                {
                    try
                    {
                        process.AddTask(LDHProcessID, directory, Directories.Find(x => x.ID == directory).SycaraID);
                    }
                    catch (DAL.DirectoryDataException dex)
                    {
                        DAL.Log.AddMessage(string.Format("Unable to pull Directory data from the database. Directory ID: {0}. Error: {1}", directory, dex.Message));
                    }
                    catch (DAL.ProcessDataException pex)
                    {
                        DAL.Log.AddMessage(string.Format("Unable to write new Task into the database. Process ID: {0}. Directory ID: {1}. Error: {2}", LDHProcessID, directory, pex.Message));
                    }
                    catch (Exception ex)
                    {
                        DAL.Log.AddMessage(string.Format("Failed to create new Task for Process ID: {0}. Directory ID: {1}. Error: {2}", LDHProcessID, directory, ex.Message));
                    }
                }
            }
            catch (Exception ex)
            {
                DAL.Log.AddMessage(string.Format("Failed to create new process. Error: {0}", ex.Message));
                throw;
            }
        }
    }
}