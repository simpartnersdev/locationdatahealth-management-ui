﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Newtonsoft.Json;

using LDH.Management.WebUI.Models;
using LDH.Management.WebUI.Models.Enums;
using LDH.Management.WebUI.Models.UI_API;

namespace LDH.Management.WebUI.BL
{
    public class BusinessType
    {
        public BTUIDataResponse GetBusinessTypeData()
        {
            var BTManager = new DAL.BusinessType();
            var BusinessTypesList = BTManager.GetBusinessTypesList();
            var BusinessTypesShortSettings = BTManager.GetBusinessTypesSettings();
            var CurrentBTs = BTManager.GetBusinessTypesFullSettingsList();
            var CurrentDomains = BTManager.GetDomainList();

            //add default values for new BT that didn't have settings already stored
            foreach (var bt in BusinessTypesList)
            {
                if (BusinessTypesShortSettings.Find(x => x.Business == bt.Name) == null)
                {
                    BusinessTypesShortSettings.Add(new Models.BusinessType()
                    {
                        Business = bt.Name,
                        Vertical = bt.Vertical.Name,
                        Domains = BTManager.GetDomainList()
                    });
                    //TODO: do I need to POST this now?
                }

                BusinessTypesShortSettings.Find(x => x.Business == bt.Name).Vertical = bt.Vertical.Name;
            }

            var BusinessTypesToDelete = new List<string>();
            var DomainsToDelete = new List<string>();
            foreach (var settings in BusinessTypesShortSettings)
            {
                //delete settings that are no longer used
                if (BusinessTypesList.Find(x => x.Name == settings.Business) == null)
                {
                    BusinessTypesToDelete.Add(settings.Business);
                }

                //add missing domains
                foreach (var domain in CurrentDomains)
                {
                    if (settings.Domains.Find(x => x.Name == domain.Name) == null)
                    {
                        settings.Domains.Add(domain);
                    }
                }

                //remove unneeded domains
                DomainsToDelete = new List<string>();
                foreach (var domain in settings.Domains)
                {
                    if (CurrentDomains.Find(x => x.Name == domain.Name) == null)
                    {
                        DomainsToDelete.Add(domain.Name);
                    }
                }

                foreach (var domain in DomainsToDelete)
                {
                    settings.Domains.Remove(settings.Domains.Find(x => x.Name == domain));
                }
            }
            foreach (var bt in BusinessTypesToDelete)
            {
                if (CurrentBTs.Find(x => x.BusinessType.Name == bt) != null)
                {
                    BTManager.DeleteBusinessTypeSettings(CurrentBTs.Find(x => x.BusinessType.Name == bt)._id);
                }
                BusinessTypesShortSettings.Remove(BusinessTypesShortSettings.Find(x => x.Business == bt));
            }

            var returnValue = DAL.BusinessType.ConvertToBTUIDataResponse(BusinessTypesShortSettings);
            return returnValue;
        }

        public void UpdateBusinessTypes(List<Models.BusinessTypeData> BusinessTypes)
        {
            var BTManager = new DAL.BusinessType();
            var CurrentBTs = BTManager.GetBusinessTypesFullSettingsList();
            var BusinessTypesShortSettings = BTManager.GetBusinessTypesSettings();
            var BusinessTypesList = BTManager.GetBusinessTypesList();
            var BTDomainData = new Models.BusinessTypesDomains();
            var BTSettings = new Models.DataTypesAPI.BusinessTypeSettings();
            var JData = string.Empty;
            var Errors = false;

            foreach (var bt in BusinessTypes)
            {
                BTDomainData = new BusinessTypesDomains()
                {
                    Domains = new List<Domain>()
                };
                BTDomainData.Domains.Add(new Domain() { Name = "Bing Local",        Weight = bt.BingLocal });
                BTDomainData.Domains.Add(new Domain() { Name = "CitySearch",        Weight = bt.CitySearch });
                BTDomainData.Domains.Add(new Domain() { Name = "ExpressUpdate",     Weight = bt.ExpressUpdate });
                BTDomainData.Domains.Add(new Domain() { Name = "Facebook",          Weight = bt.Facebook });
                BTDomainData.Domains.Add(new Domain() { Name = "Foursquare",        Weight = bt.Foursquare });
                BTDomainData.Domains.Add(new Domain() { Name = "GoogleMyBusiness",  Weight = bt.GoogleMyBusiness });
                BTDomainData.Domains.Add(new Domain() { Name = "Local",             Weight = bt.Local });
                BTDomainData.Domains.Add(new Domain() { Name = "MapQuest",          Weight = bt.MapQuest });
                BTDomainData.Domains.Add(new Domain() { Name = "MerchantCircle",    Weight = bt.MerchantCircle });
                BTDomainData.Domains.Add(new Domain() { Name = "TripAdvisor",       Weight = bt.TripAdvisor });
                BTDomainData.Domains.Add(new Domain() { Name = "YahooLocal",        Weight = bt.YahooLocal });
                BTDomainData.Domains.Add(new Domain() { Name = "YellowPages",       Weight = bt.YellowPages });
                BTDomainData.Domains.Add(new Domain() { Name = "Yelp",              Weight = bt.Yelp });
                BTDomainData.Domains.Add(new Domain() { Name = "HealthGrades",      Weight = bt.HealthGrades });
                BTDomainData.Domains.Add(new Domain() { Name = "MDcom",            Weight = bt.MDcom });

                BTSettings = CurrentBTs.Find(x => x.BusinessType.Name == bt.BusinessType);
                if (BTSettings != null)
                {
                    BTSettings.SettingsJson = JsonConvert.SerializeObject(BTDomainData);
                    BTManager.UpdateBusinessTypeSettings(BTSettings);
                }
                else
                {
                    //add new Business Type
                    var BusinessType = BusinessTypesList.Find(x => x.Name == bt.BusinessType);
                    if (BusinessType != null)
                    {
                        BTSettings = new Models.DataTypesAPI.BusinessTypeSettings()
                        {
                            Application = BTManager.GetApplication(),
                            Application_id = BTManager.GetApplication()._id,
                            BusinessType = BusinessType,
                            BusinessType_id = BusinessType._id,
                            Name = BTManager.GetBusinessTypeSettingsName(bt.BusinessType),
                            ComponentName = BTManager.GetComponentName(),
                            SettingsJson = JsonConvert.SerializeObject(BTDomainData)
                        };

                        BTManager.CreateBusinessTypeSettings(BTSettings);
                    }
                    else
                    {
                        Errors = true;
                        DAL.Log.AddMessage(string.Format("Business Type not found ({}), unable to create settings for this Business Type.", bt.BusinessType));
                    }
                }
            }

            if (Errors)
            {
                throw new Exception("There were errors while trying to save the Business Types Settings. Please ask the Web Site administrator to review the log file to get the error details.");
            }
        }

        public void CalculateBusinessTypeAltScore()
        {
            if (!new DAL.BusinessType().StartBusinessSettingsWorkerProcess())
            {
                throw new Exception("The system is still processing alternative scores. Please try again later.");
            }
        }

        public int GetWorkerServiceStatus()
        {
            return new DAL.BusinessType().GetWorkerServiceStatus();
        }
    }
}