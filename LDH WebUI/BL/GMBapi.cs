﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using LDH.Management.WebUI.Models.GMBapi;

namespace LDH.Management.WebUI.BL
{
    public class GMBapi
    {
        public GMBapiSearchResponse GMBSearch(GMBapiSearchRequest req)
        {
            var GMBobj = new DAL.GMBapi.GMBapi();
            return new GMBapiSearchResponse() { Doctors = GMBobj.GetLocations(req.Name, req.City, req.State) };
        }
    }
}