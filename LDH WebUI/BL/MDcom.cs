﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using LDH.Management.WebUI.Models.MDcom;

namespace LDH.Management.WebUI.BL
{
    public class MDcom
    {
        public MDcomSearchResponse MDSearch(MDcomSearchRequest req)
        {
            var MDobj = new DAL.MDcom.MDcom();
            return new MDcomSearchResponse() { Doctors = MDobj.GetDoctors(req.Name, req.City, req.State) };
        }
    }
}