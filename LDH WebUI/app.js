﻿//var app = angular.module('app', ['ngTouch', 'ui.grid', 'ui.grid.edit']);
//var obj = [{ "domain": "Google My Business", "Clinic": "40.0", "Hospital": "50.0", "Hotel": "60.0", "Auto": 70 }, { "domain": "Yahoo Local", "Clinic": "30.0", "Hospital": "20.0", "Hotel": "20.0", "Auto": 10.5 }, { "domain": "Bing Local", "Clinic": "40.0", "Hospital": "50.0", "Hotel": "60.0", "Auto": 20 }];

//var localData = JSON.stringify(obj);
///*
//$.ajax({
//    method: "GET",
//    url: "/ui_api/getbusinesstypedata",
//    contentType: 'application/json',
//    dataType: "json",
//    async: false,
//    success: function (data) {
//        localData = data.GridData;
//    }
//});
//*/
//app.controller('MainCtrl', ['$scope', function ($scope) {
//    //$scope.gridOptions.columnDefs = [
//    //    { name: 'domain', enableCellEdit: false },
//    //    { name: 'Clinic', enableCellEdit: true, type: 'number' },
//    //    { name: 'Hospital', enableCellEdit: true, type: 'number' },
//    //    { name: 'Hotel', enableCellEdit: true, type: 'number' }
//    //];
//    //$scope.gridOptions.data = JSON.parse(localData);
//    $scope.myData = JSON.parse(localData);
//    $scope.
//}]);


//function sendBTData()
//{

//}







var app = angular.module('app', ['ngTouch', 'ui.grid', 'ui.grid.edit', 'addressFormatter']);

angular.module('addressFormatter', []).filter('address', function () {
    return function (input) {
        return input.street + ', ' + input.city + ', ' + input.state + ', ' + input.zip;
    };
});

app.controller('MainCtrl', ['$scope', '$http', '$timeout', function ($scope, $http, $timeout) {
    $scope.gridOptions = {};


    $scope.storeFile = function (gridRow, gridCol, files) {
        // ignore all but the first file, it can only select one anyway
        // set the filename into this column
        gridRow.entity.filename = files[0].name;

        // read the file and set it into a hidden column, which we may do stuff with later
        var setFile = function (fileContent) {
            gridRow.entity.file = fileContent.currentTarget.result;
            // put it on scope so we can display it - you'd probably do something else with it
            $scope.lastFile = fileContent.currentTarget.result;
            $scope.$apply();
        };
        var reader = new FileReader();
        reader.onload = setFile;
        reader.readAsText(files[0]);
    };
    /*
    $scope.gridOptions.columnDefs = [
      { name: 'id', enableCellEdit: true, width: '10%' },
      
    ];
    */
    $scope.msg = {};

    $scope.gridOptions.onRegisterApi = function (gridApi) {
        //set gridApi on scope
        $scope.gridApi = gridApi;
        gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
            $scope.msg.lastCellEdited = 'edited row id:' + rowEntity.id + ' Column:' + colDef.name + ' newValue:' + newValue + ' oldValue:' + oldValue;
            $scope.$apply();
        });
    };

    $http.get('/ui_api/getbusinesstypedata')
      .success(function (data) {
          //for (i = 0; i < data.length; i++) {
              //data[i].registered = new Date(data[i].registered);
              //data[i].gender = data[i].gender === 'male' ? 1 : 2;
              //if (i % 2) {
              //    data[i].pet = 'fish'
              //    data[i].foo = { bar: [{ baz: 2, options: [{ value: 'fish' }, { value: 'hamster' }] }] }
              //}
              //else {
              //    data[i].pet = 'dog'
              //    data[i].foo = { bar: [{ baz: 2, options: [{ value: 'dog' }, { value: 'cat' }] }] }
              //}
          //}
          $scope.gridOptions.data = data;
      });
}])
;