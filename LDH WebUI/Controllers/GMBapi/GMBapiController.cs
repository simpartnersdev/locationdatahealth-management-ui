﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

using LDH.Management.WebUI.Models.GMBapi;

namespace LDH.Management.WebUI.Controllers.GMBapi
{
    public class GMBapiController : ApiController
    {
        [HttpPost]
        [Route("gmb_api/Search")]
        [AllowAnonymous]
        public GMBapiSearchResponse GMBSearch(GMBapiSearchRequest req)
        {
            var GMBobj = new BL.GMBapi();
            return GMBobj.GMBSearch(req);
        }
    }
}