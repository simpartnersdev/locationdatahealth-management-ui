﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using LDH.Management.WebUI.Models.Enums;
using LDH.Management.WebUI.Models.UI_API;

namespace LDH.Management.WebUI.Controllers.BusinessType
{
    public class BusinessTypeDataController : ApiController
    {
        // GET: DirectoriesData
        [HttpGet]
        [Route("ui_api/getbusinesstypedata")]
        public BTUIDataResponse GetBusinessTypeData()
        {
            return new BL.BusinessType().GetBusinessTypeData();
        }

        [HttpGet]
        [Route("ui_api/getworkerservicestatus")]
        public string GetWorkerServiceStatus()
        {
            var Progress = new BL.BusinessType().GetWorkerServiceStatus();
            var Status = string.Empty;

            if (Progress == 0)
            {
                Status = "Not Running";
            }
            else if (Progress == 100)
            {
                Status = "Calculations Completed";
            }
            else if (Progress > 0 && Progress < 100)
            {
                Status = string.Format("Working ({0}%)", Progress);
            }
            else
            {
                Status = string.Format("WTH?! ({0}%)", Progress);
            }
            return Status;
        }

        [HttpPost]
        [Route("ui_api/setbusinesstypedata")]
        public void SetBusinessTypeData(List<Models.BusinessTypeData> BusinessTypes)
        {
            new BL.BusinessType().UpdateBusinessTypes(BusinessTypes);
        }

        [HttpPost]
        [Route("ui_api/calculatealtscores")]
        public void CalculateAlternativeScoreData()
        {
            new BL.BusinessType().CalculateBusinessTypeAltScore();
        }
    }
}
