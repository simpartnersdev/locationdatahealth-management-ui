﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using LDH.Management.WebUI.Models.MDcom;

namespace LDH.Management.WebUI.Controllers.MDcom
{
    public class MDcomController : ApiController
    {
        [HttpPost]
        [Route("mdcom_api/Search")]
        [AllowAnonymous]
        public MDcomSearchResponse MDSearch(MDcomSearchRequest req)
        {
            var MDobj = new BL.MDcom();
            return MDobj.MDSearch(req);
        }
    }
}
