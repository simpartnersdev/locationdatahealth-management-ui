﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using LDH.Management.WebUI.Models.Enums;
using LDH.Management.WebUI.Models.UI_API;

namespace LDH.Management.WebUI.Controllers.Directory
{
    public class DirectoryDataController : ApiController
    {
        // GET: DirectoriesData
        [HttpGet]
        [Route("ui_api/getdirectorydata")]
        public DirectoryDataResponse GetDirectoryData()
        {
            return new BL.Directory().GetDirectoriesData(DirectoryStatus.Active);
        }

        [HttpPost]
        [Route("ui_api/exportcsv")]
        public void ExportCSV(Models.CSVExportSettings ExportSettings)
        {
            if (ExportSettings != null)
            {
                new BL.Directory().GenerateCSVFiles(ExportSettings);
            }
        }

        [HttpGet]
        [Route("ui_api/getprocesslist")]
        public ProcessListResponse GetProcessList()
        {
            return new BL.Directory().GetProcessList();
        }
    }
}
