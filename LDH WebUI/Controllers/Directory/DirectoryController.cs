﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using LDH.Management.WebUI.Models;
using LDH.Management.WebUI.Models.Enums;

namespace LDH.Management.WebUI.Controllers.Account
{
    public class DirectoryController : Controller
    {
        // GET: Account
        public ActionResult Index()
        {
            //List<Models.Directory> accounts = new BL.Directory().GetDirectories(DirectoryStatus.Active);
            List<Models.Directory> dummy = new List<Models.Directory>();
            return View("WorkStatus", dummy);
        }

        [HttpPost]
        public ActionResult LaunchLDH(List<int> Directories)
        {
            new BL.Process().StartProcess(Directories);
            return new EmptyResult();
        }
    }
}