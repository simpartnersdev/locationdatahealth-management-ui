﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH.Management.WebUI.Models
{
    public class BusinessTypeData
    {
        public string DT_RowId { get; set; }
        public string BusinessType { get; set; }
        public string Vertical { get; set; }
        public decimal BingLocal { get; set; }
        public decimal CitySearch { get; set; }
        public decimal ExpressUpdate { get; set; }
        public decimal Facebook { get; set; }
        public decimal Foursquare { get; set; }
        public decimal GoogleMyBusiness { get; set; }
        public decimal Local { get; set; }
        public decimal MapQuest { get; set; }
        public decimal MerchantCircle { get; set; }
        public decimal TripAdvisor { get; set; }
        public decimal YahooLocal { get; set; }
        public decimal YellowPages { get; set; }
        public decimal Yelp { get; set; }
        public decimal HealthGrades { get; set; }
        public decimal MDcom { get; set; }
    }
}