﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH.Management.WebUI.Models
{
    public class CSVExportSettings
    {
        public DateTime ExportDate { get; set; }
        public List<CSVDirectoryProcess> Directories { get; set; }
        public bool ExportToS3 { get; set; }
    }

    public class CSVDirectoryProcess
    {
        public int DirectoryID { get; set; }
        public int ProcessID { get; set; }
    }
}