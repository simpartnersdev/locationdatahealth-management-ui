﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH.Management.WebUI.Models.UI_API
{
    public class BusinessTypeDataResponse
    {
        public List<string> AvailableBusinessTypes { get; set; }
        public string GridData { get; set; }
        public List<BusinessTypeSettings> CurrentSettings { get; set; }
    }
}