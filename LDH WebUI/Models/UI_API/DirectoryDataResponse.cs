﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH.Management.WebUI.Models.UI_API
{
    public class DirectoryDataResponse
    {
        public List<Models.Directory> data { get; set; }
    }
}