﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH.Management.WebUI.Models
{
    public class BusinessTypesServiceResponse
    {
        public List<BusinessTypeSettings> Settings { get; set; }
    }

    public class BusinessTypeSettings
    {
        public string BusinessType { get; set; }
        public string Settings { get; set; }
    }

    public class BusinessTypesDomains
    {
        public List<Domain> Domains;
    }
}