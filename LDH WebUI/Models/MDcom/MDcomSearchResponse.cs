﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH.Management.WebUI.Models.MDcom
{
    public class MDcomSearchResponse
    {
        public List<MDcomDoctor> Doctors { get; set; }
    }

    public class MDcomDoctor
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string State { get; set; }
        public string Website { get; set; }
        public string MDcomID { get; set; }
        public string MDcomURL { get; set; }
    }
}