﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH.Management.WebUI.Models.Enums
{
    public enum DirectoryStatus
    {
        Active = 1,
        Inactive = 0,
    }
}