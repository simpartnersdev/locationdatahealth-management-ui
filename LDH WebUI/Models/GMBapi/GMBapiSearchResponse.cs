﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH.Management.WebUI.Models.GMBapi
{
    public class GMBapiSearchResponse
    {
        public List<GMBapiLocation> Doctors { get; set; }
    }

    public class GMBapiLocation
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string State { get; set; }
        public string Website { get; set; }
        public string LocationID { get; set; }
        public string LocationURL { get; set; }
    }
}