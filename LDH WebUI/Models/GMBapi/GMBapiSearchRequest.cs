﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH.Management.WebUI.Models.GMBapi
{
    public class GMBapiSearchRequest
    {
        public string Name { get; set; }
        public string City { get; set; }
        public string State { get; set; }
    }
}