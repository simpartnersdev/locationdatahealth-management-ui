﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH.Management.WebUI.Models.DataTypesAPI
{
    public class Application
    {
        public string _id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string GlobalSchema { get; set; }
        public List<Schema> Schemas { get; set; }
    }
}