﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH.Management.WebUI.Models.DataTypesAPI
{
    public class BusinessType
    {
        public string _id { get; set; }
        public string Vertical_id { get; set; }
        public string MetaType_id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public MetaType MetaType { get; set; }
        public MetaType Vertical { get; set; }
    }
}