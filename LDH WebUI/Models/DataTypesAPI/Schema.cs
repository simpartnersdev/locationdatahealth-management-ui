﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH.Management.WebUI.Models.DataTypesAPI
{
    public class Schema
    {
        public string ComponentName { get; set; }
        public string Jschema { get; set; }
    }
}