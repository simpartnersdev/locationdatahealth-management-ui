﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH.Management.WebUI.Models.DataTypesAPI
{
    public class BusinessTypeSettings
    {
        public string _id { get; set; }
        public string GlobalSettings_id { get; set; }
        public string BusinessType_id { get; set; }
        public string Application_id { get; set; }
        public string Vertical_id { get; set; }
        public string MetaType_id { get; set; }
        public string Name { get; set; }
        public string ComponentName { get; set; }
        public string CustomIdentifier { get; set; }
        public string CustomValue { get; set; }
        public string SettingsJson { get; set; }
        public BusinessType BusinessType { get; set; }
        public Application Application { get; set; }
        public MetaType MetaType { get; set; }
        public MetaType Vertical { get; set; }
    }
}