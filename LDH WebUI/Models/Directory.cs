﻿using System.ComponentModel;
using System.Data.Linq.Mapping;

using LDH.Management.WebUI.Models.Enums;

namespace LDH.Management.WebUI.Models
{
    public class Directory
    {
        [Column(Name = "account_id")]
        [DisplayName("Sycara ID")]
        public int SycaraID { get; set; }

        [Column(Name = "account_name")]
        public string Name { get; set; }

        [Column(Name = "api_key")]
        [DisplayName("API Key")]
        public string ApiKey { get; set; }

        [Column(Name = "account_status", DbType = "int")]
        public DirectoryStatus Status { get; set; }

        [Column(Name = "velocity_directory_id")]
        public int ID { get; set; }

        [Column(Name = "total_profiles")]
        [DisplayName("Profile Count")]
        public int ProfileCount { get; set; }

        [Column(Name = "is_queued")]
        public bool IsQueued { get; set; }

        [Column(Name = "queued_jobs")]
        public int QueuedJobs { get; set; }

        [Column(Name = "active_jobs")]
        public int ActiveJobs { get; set; }

        [Column(Name = "completed_jobs")]
        public int CompletedJobs { get; set; }
    }
}
