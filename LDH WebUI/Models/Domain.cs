﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace LDH.Management.WebUI.Models
{
    public class Domain
    {
        [Key]
        public string Name { get; set; }
        public decimal Weight { get; set; }
    }
}