﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace LDH.Management.WebUI.Models
{
    public class BusinessType
    {
        [Key]
        public string Business { get; set; }
        public string Vertical { get; set; }
        public List<Domain> Domains { get; set; }
    }    
}