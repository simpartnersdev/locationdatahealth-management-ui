﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace LDH.Management.WebUI.DAL
{
    public static class Utils
    {
        public static List<T> ToList<T>(this DataTable Table) where T : class, new()
        {
            string strColumn = string.Empty;
            try
            {
                List<T> list = new List<T>();

                T obj;
                PropertyInfo propertyInfo;

                foreach (var row in Table.AsEnumerable())
                {
                    obj = new T();

                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        if (prop.CustomAttributes.ToList().Exists(x => x.AttributeType.Name == "ColumnAttribute" &&
                                                                       x.NamedArguments.ToList().Exists(y => y.MemberName == "IsDbGenerated") &&
                                                                       x.NamedArguments.First(z => z.MemberName == "IsDbGenerated").TypedValue.Value.ToString().ToLower() == "false"))
                        {
                            continue;
                        }

                        try
                        {
                            if (prop.CustomAttributes.ToList().Exists(x => x.AttributeType.Name == "ColumnAttribute" && x.NamedArguments.ToList().Exists(y => y.MemberName == "Name")))
                            {
                                strColumn = prop.GetCustomAttributesData().First(x => x.AttributeType.Name == "ColumnAttribute").NamedArguments.First(y => y.MemberName == "Name").TypedValue.Value.ToString();
                            }
                            else
                            {
                                strColumn = prop.Name;
                            }

                            propertyInfo = obj.GetType().GetProperty(prop.Name);
                            if (prop.PropertyType.Equals(typeof(bool)) && prop.CustomAttributes.ToList().Exists(x => x.AttributeType.Name == "ColumnAttribute" &&
                                                                                                                     x.NamedArguments.ToList().Exists(y => y.MemberName == "DbType") &&
                                                                                                                     x.NamedArguments.First(z => z.MemberName == "DbType").TypedValue.Value.ToString().ToLower() == "varchar(1)"))
                            {
                                propertyInfo.SetValue(obj, row[strColumn].ToString().ToUpper() == "Y" || row[strColumn].ToString().ToUpper() == "T" ? true : false, null);
                            }
                            else if (prop.PropertyType.IsEnum)
                            {
                                propertyInfo.SetValue(obj, Enum.Parse(Type.GetType(propertyInfo.PropertyType.FullName), row[strColumn].ToString(), true));
                            }
                            else
                            {
                                propertyInfo.SetValue(obj, Convert.ChangeType(row[strColumn], propertyInfo.PropertyType), null);
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }

                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }

        public static string GetRedshiftConnectionString()
        {
            //determine driver version
            string driverVersion = "x86";
            if (IntPtr.Size == 8)
                driverVersion = "x64";

            return string.Format(ConfigurationManager.ConnectionStrings["RedshiftConnection"].ConnectionString, driverVersion);
        }

        public static string ToCsv(this List<string> List)
        {
            StringBuilder sbCSV = new StringBuilder();
            foreach (var value in List)
            {
                sbCSV.Append(string.Format(",'{0}'", value));
            }

            if (sbCSV.Length == 0)
            {
                return string.Empty;
            }

            return sbCSV.ToString().Substring(1);
        }

        public static string ToCsv(this List<int> List)
        {
            StringBuilder sbCSV = new StringBuilder();
            foreach (var value in List)
            {
                sbCSV.Append(string.Format(",{0}", value));
            }

            if (sbCSV.Length == 0)
            {
                return string.Empty;
            }

            return sbCSV.ToString().Substring(1);
        }

        public static string gzUncompressText(Stream data)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                data.CopyTo(ms);

                using (var mem = new MemoryStream())
                {
                    //write magic number (it's called like that) in the header of the stream
                    mem.Write(new byte[] { 0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00 }, 0, 8);
                    mem.Write(ms.ToArray(), 0, ms.ToArray().Length);

                    mem.Position = 0;

                    //now uncompress the thing
                    using (var gzip = new GZipStream(mem, CompressionMode.Decompress))
                    using (var reader = new StreamReader(gzip))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }
    }
}