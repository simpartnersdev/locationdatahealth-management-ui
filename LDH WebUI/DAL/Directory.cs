﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;

using Amazon.S3;
using Amazon.S3.Model;

using MySql.Data.MySqlClient;

using LDH.Management.WebUI.Models.Enums;

namespace LDH.Management.WebUI.DAL
{
    public class Directory
    {
        private static IAmazonS3 S3Client = new AmazonS3Client();

        public List<Models.Directory> GetDirectories(DirectoryStatus Status)
        {
            var result = new List<Models.Directory>();
            var conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["LDH_MySQL"].ConnectionString);
            var da = new MySqlDataAdapter("get_Directories", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("directory_status", (int)Status);

            var dt = new DataTable();

            try
            {
                conn.Open();
                da.Fill(dt);
                result = dt.ToList<Models.Directory>();
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }

            return result;
        }

        public void ExportCSVAccuracyData(int DirectoryID, int ProcessID, DateTime ExportDate, bool ExportToS3)
        {
            var CSVFolder = new DirectoryInfo(ConfigurationManager.AppSettings["CSVExportFolder"]);
            if (!CSVFolder.Exists)
            {
                CSVFolder.Create();
            }
            var filePath = Path.Combine(CSVFolder.FullName, string.Format("full_listing_data_{0}_{1}.csv", DirectoryID, ExportDate.ToString("yyyy_MM_dd")));
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            var CSVFile = File.AppendText(filePath);

            var conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["LDH_MySQL"].ConnectionString);
            var cmd = new MySqlCommand("get_ExportCSVListingAccuracyData", conn);
            
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "directory_id", DbType = DbType.Int32, Direction = ParameterDirection.Input, Value = DirectoryID });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "process_id", DbType = DbType.Int32, Direction = ParameterDirection.Input, Value = ProcessID });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "export_date", DbType = DbType.DateTime, Direction = ParameterDirection.Input, Value = ExportDate });

            try
            {
                conn.Open();
                var reader = cmd.ExecuteReader();
                var sbRow = new StringBuilder();
                var dataRow = new List<string>();
                

                //crete the header
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    dataRow.Add(string.Concat("\"", reader.GetName(i).Replace("\"", "\"\""), "\""));
                }
                CSVFile.WriteLine(string.Join(",", dataRow));

                //now add the data rows                
                while (reader.Read())
                {
                    dataRow.Clear();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        dataRow.Add(string.Concat("\"", reader[i].ToString().Replace("\"", "\"\""), "\""));
                    }
                    CSVFile.WriteLine(string.Join(",", dataRow));
                }
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
                CSVFile.Flush();
                CSVFile.Close();

                if (ExportToS3)
                {
                    UploadFileToS3(filePath);
                }
            }
        }

        private void UploadFileToS3(string FilePath)
        {
            try
            {
                var FileName = Path.GetFileName(FilePath);
                var upReq = new PutObjectRequest()
                {
                    BucketName = ConfigurationManager.AppSettings["S3BucketPath"],
                    Key = FileName,
                    FilePath = FilePath
                };
                var upResp = S3Client.PutObject(upReq);
            }
            catch (Exception ex)
            {
                DAL.Log.AddMessage(string.Format("Unable to upload the file {0} to S3. Error: {1}", FilePath, ex.Message));
            }
        }

        public List<Models.Process> GetProcessList()
        {
            var result = new List<Models.Process>();
            var conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["LDH_MySQL"].ConnectionString);
            var da = new MySqlDataAdapter("prc_GetProcesses", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            var dt = new DataTable();

            try
            {
                conn.Open();
                da.Fill(dt);
                result = dt.ToList<Models.Process>();
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }

            return result;
        }
    }
}