﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;

namespace LDH.Management.WebUI.DAL
{
    public static class Log
    {
        public static void AddMessage(string Text)
        {
            var Message = string.Format("{0} :: WorkerService - Thread {1} :: Message: {2}\r\n", DateTime.Now.ToString("o"), System.Threading.Thread.CurrentThread.ManagedThreadId, Text);
            try
            {
                File.AppendAllText(ConfigurationManager.AppSettings["LogFile"], Message);
            }
            catch (Exception ex)
            {
                var Source = "LDH Worker Service";
                var LogName = "Application";

                if (!EventLog.SourceExists(Source))
                    EventLog.CreateEventSource(Source, LogName);

                Message = string.Format("An error occurred while attempting to log a message. The message is: {0}. The error is: {1}", Message, ex.Message);
                EventLog.WriteEntry(Source, Message, EventLogEntryType.Error);
            }

        }
    }
}