﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;

using MySql.Data.MySqlClient;

using LDH.Management.WebUI.Models.GMBapi;

namespace LDH.Management.WebUI.DAL.GMBapi
{
    public class GMBapi
    {
        public List<GMBapiLocation> GetLocations(string Name, string City, string State)
        {
            var result = new List<GMBapiLocation>();
            var conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["LDH_MySQL"].ConnectionString);
            var da = new MySqlDataAdapter("get_GoogleMyBusinessAPILocations", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("name", Name);
            da.SelectCommand.Parameters.AddWithValue("city", City);
            da.SelectCommand.Parameters.AddWithValue("state", State);

            var dt = new DataTable();

            try
            {
                conn.Open();
                da.Fill(dt);
                result = dt.ToList<GMBapiLocation>();
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }

            return result;
        }
    }
}