﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;

using MySql.Data.MySqlClient;

using Newtonsoft.Json;

using LDH.Management.WebUI.Models.UI_API;

namespace LDH.Management.WebUI.DAL
{
    public class BusinessType
    {
        public void DeleteBusinessTypeSettings(string ID)
        {
            var resp = string.Empty;
            var Payload = JsonConvert.SerializeObject(ID);

            var request = (HttpWebRequest)HttpWebRequest.Create(ConfigurationManager.AppSettings["BusinessTypesSettingsPost"] + "Delete?id=" + ID);
            request.Headers.Add("Token", VelocityToken.Token);
            request.Method = "DELETE";
            
            request.BeginGetResponse((x) =>
            {
                using (HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(x))
                {
                    if (response.ContentEncoding == "gzip")
                    {
                        resp = Utils.gzUncompressText(response.GetResponseStream());
                    }
                    else
                    {
                        TextReader tr = new StreamReader(response.GetResponseStream());
                        resp = tr.ReadToEnd();
                    }
                }
            }, null);
        }

        public void CreateBusinessTypeSettings(Models.DataTypesAPI.BusinessTypeSettings BusinessTypeSettings)
        {
            var resp = string.Empty;
            var Payload = JsonConvert.SerializeObject(BusinessTypeSettings);

            var request = (HttpWebRequest)HttpWebRequest.Create(ConfigurationManager.AppSettings["BusinessTypesSettingsPost"]);
            request.Headers.Add("Token", VelocityToken.Token);
            request.Method = "POST";
            request.ContentType = "application/json";

            using (var sw = new StreamWriter(request.GetRequestStream()))
            {
                sw.Write(Payload);
                sw.Flush();
                sw.Close();
            }

            request.BeginGetResponse((x) =>
            {
                using (HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(x))
                {
                    if (response.ContentEncoding == "gzip")
                    {
                        resp = Utils.gzUncompressText(response.GetResponseStream());
                    }
                    else
                    {
                        TextReader tr = new StreamReader(response.GetResponseStream());
                        resp = tr.ReadToEnd();
                    }
                }
            }, null);
        }

        public List<Models.BusinessType> GetBusinessTypesSettings()
        {
            var data = string.Empty;
            var request = (HttpWebRequest)HttpWebRequest.Create(ConfigurationManager.AppSettings["BusinessTypesSettingsService"] + string.Format("?Application={0}&Component={1}", GetApplicationName(), GetComponentName()));
            request.Headers.Add("Token", VelocityToken.Token);
            request.Method = "GET";

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                    TextReader tr = new StreamReader(response.GetResponseStream());
                    data = tr.ReadToEnd();
            }

            var BTServiceResponse = JsonConvert.DeserializeObject<Models.BusinessTypesServiceResponse>(data);
            var BusinessTypes = new List<Models.BusinessType>();
            var tmpBT = new Models.BusinessType();

            foreach (var BT in BTServiceResponse.Settings)
            {
                tmpBT = new Models.BusinessType()
                {
                    Business = BT.BusinessType,
                    Domains = JsonConvert.DeserializeObject<Models.BusinessTypesDomains>(BT.Settings).Domains
                };
                //TODO: remove this nasty fix once the BT service fixes the names of the domains
                for (int i = 0; i < tmpBT.Domains.Count; i++)
                {
                    tmpBT.Domains[i].Name = tmpBT.Domains[i].Name.Replace(" ", string.Empty);
                }

                BusinessTypes.Add(tmpBT);
            }
            return BusinessTypes;
        }

        public List<Models.DataTypesAPI.BusinessType> GetBusinessTypesList()
        {
            var data = string.Empty;
            var request = (HttpWebRequest)HttpWebRequest.Create(ConfigurationManager.AppSettings["BusinessTypesListService"]);
            request.Headers.Add("Token", VelocityToken.Token);
            request.Method = "GET";

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                TextReader tr = new StreamReader(response.GetResponseStream());
                data = tr.ReadToEnd();
            }

            var BTServiceResponse = JsonConvert.DeserializeObject<List<Models.DataTypesAPI.BusinessType>>(data);

            return BTServiceResponse;
        }

        public List<Models.DataTypesAPI.BusinessTypeSettings> GetBusinessTypesFullSettingsList()
        {
            var data = string.Empty;
            var request = (HttpWebRequest)HttpWebRequest.Create(ConfigurationManager.AppSettings["BusinessTypesSettingsGet"] + string.Format("?Application={0}&Component={1}", GetApplicationName(), GetComponentName()));
            request.Headers.Add("Token", VelocityToken.Token);
            request.Method = "GET";

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                TextReader tr = new StreamReader(response.GetResponseStream());
                data = tr.ReadToEnd();
            }

            var BTServiceResponse = JsonConvert.DeserializeObject<List<Models.DataTypesAPI.BusinessTypeSettings>>(data);
            
            return BTServiceResponse;
        }

        public void UpdateBusinessTypeSettings(Models.DataTypesAPI.BusinessTypeSettings BusinessTypeSettings)
        {
            var resp = string.Empty;
            var Payload = JsonConvert.SerializeObject(BusinessTypeSettings);

            var request = (HttpWebRequest)HttpWebRequest.Create(ConfigurationManager.AppSettings["BusinessTypesSettingsPost"]);
            request.Headers.Add("Token", VelocityToken.Token);
            request.Method = "POST";
            request.ContentType = "application/json";

            using (var sw = new StreamWriter(request.GetRequestStream()))
            {
                sw.Write(Payload);
                sw.Flush();
                sw.Close();
            }

            request.BeginGetResponse((x) =>
            {
                using (HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(x))
                {
                    if (response.ContentEncoding == "gzip")
                    {
                        resp = Utils.gzUncompressText(response.GetResponseStream());
                    }
                    else
                    {
                        TextReader tr = new StreamReader(response.GetResponseStream());
                        resp = tr.ReadToEnd();
                    }
                }
            }, null);
        }

        public static BTUIDataResponse ConvertToBTUIDataResponse(List<Models.BusinessType> BusinessTypes)
        {
            var returnValue = new BTUIDataResponse();
            returnValue.data = new List<Models.BusinessTypeData>();

            foreach (var bt in BusinessTypes)
            {
                returnValue.data.Add(new Models.BusinessTypeData()
                {
                    //TODO: this is a hard-coded list of domains because the DataTable in the UI requires a non-dynamic set of columns,
                    //we should build a workaround to make it flexible
                    DT_RowId = bt.Business,
                    BusinessType = bt.Business,
                    Vertical = bt.Vertical,
                    BingLocal = bt.Domains.Find(x => x.Name == "BingLocal") == null ? 0m : bt.Domains.Find(x => x.Name == "BingLocal").Weight,
                    CitySearch = bt.Domains.Find(x => x.Name == "CitySearch") == null ? 0m : bt.Domains.Find(x => x.Name == "CitySearch").Weight,
                    ExpressUpdate = bt.Domains.Find(x => x.Name == "ExpressUpdate") == null ? 0m : bt.Domains.Find(x => x.Name == "ExpressUpdate").Weight,
                    Facebook = bt.Domains.Find(x => x.Name == "Facebook") == null ? 0m : bt.Domains.Find(x => x.Name == "Facebook").Weight,
                    Foursquare = bt.Domains.Find(x => x.Name == "Foursquare") == null ? 0m : bt.Domains.Find(x => x.Name == "Foursquare").Weight,
                    GoogleMyBusiness = bt.Domains.Find(x => x.Name == "GoogleMyBusiness") == null ? 0m : bt.Domains.Find(x => x.Name == "GoogleMyBusiness").Weight,
                    Local = bt.Domains.Find(x => x.Name == "Local") == null ? 0m : bt.Domains.Find(x => x.Name == "Local").Weight,
                    MapQuest = bt.Domains.Find(x => x.Name == "MapQuest") == null ? 0m : bt.Domains.Find(x => x.Name == "MapQuest").Weight,
                    MerchantCircle = bt.Domains.Find(x => x.Name == "MerchantCircle") == null ? 0m : bt.Domains.Find(x => x.Name == "MerchantCircle").Weight,
                    TripAdvisor = bt.Domains.Find(x => x.Name == "TripAdvisor") == null ? 0m : bt.Domains.Find(x => x.Name == "TripAdvisor").Weight,
                    YahooLocal = bt.Domains.Find(x => x.Name == "YahooLocal") == null ? 0m : bt.Domains.Find(x => x.Name == "YahooLocal").Weight,
                    YellowPages = bt.Domains.Find(x => x.Name == "YellowPages") == null ? 0m : bt.Domains.Find(x => x.Name == "YellowPages").Weight,
                    Yelp = bt.Domains.Find(x => x.Name == "Yelp") == null ? 0m : bt.Domains.Find(x => x.Name == "Yelp").Weight,
                    HealthGrades = bt.Domains.Find(x => x.Name == "HealthGrades") == null ? 0m : bt.Domains.Find(x => x.Name == "HealthGrades").Weight,
                    MDcom = bt.Domains.Find(x => x.Name == "MDcom") == null ? 0m : bt.Domains.Find(x => x.Name == "MDcom").Weight
                });
            }

            return returnValue;
        }

        public bool StartBusinessSettingsWorkerProcess()
        {
            var returnValue = false;
            var oCon = new SqlConnection(ConfigurationManager.ConnectionStrings["LDH_SQL"].ConnectionString);
            var oCmd = new SqlCommand("bts.bts_StartWork", oCon);
            oCmd.CommandType = CommandType.StoredProcedure;
            var CurrentStep = new SqlParameter() { ParameterName = "@CurrentStep", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output };
            oCmd.Parameters.Add(CurrentStep);

            try
            {
                oCon.Open();
                oCmd.ExecuteNonQuery();

                returnValue = (int)CurrentStep.Value == 0;
            }
            finally
            {
                if (oCon.State != ConnectionState.Closed)
                {
                    oCon.Close();
                }
            }

            return returnValue;
        }

        public int GetWorkerServiceStatus()
        {
            var returnValue = 0;
            var oCon = new SqlConnection(ConfigurationManager.ConnectionStrings["LDH_SQL"].ConnectionString);
            var oCmd = new SqlCommand("bts.bts_GetProgress", oCon);
            oCmd.CommandType = CommandType.StoredProcedure;
            var Progress = new SqlParameter() { ParameterName = "@Progress", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output };
            oCmd.Parameters.Add(Progress);

            try
            {
                oCon.Open();
                oCmd.ExecuteNonQuery();

                returnValue = (int)Progress.Value;
            }
            finally
            {
                if (oCon.State != ConnectionState.Closed)
                {
                    oCon.Close();
                }
            }

            return returnValue;
        }

        public List<Models.Domain> GetDomainList()
        {
            //TODO: this is a hard-coded list of domains, in the future we should make this flexible
            var returnValue = new List<Models.Domain>();

            returnValue.Add(new Models.Domain() { Name = "BingLocal", Weight = 0 });
            returnValue.Add(new Models.Domain() { Name = "CitySearch", Weight = 0 });
            returnValue.Add(new Models.Domain() { Name = "ExpressUpdate", Weight = 0 });
            returnValue.Add(new Models.Domain() { Name = "Facebook", Weight = 0 });
            returnValue.Add(new Models.Domain() { Name = "Foursquare", Weight = 0 });
            returnValue.Add(new Models.Domain() { Name = "GoogleMyBusiness", Weight = 0 });
            returnValue.Add(new Models.Domain() { Name = "Local", Weight = 0 });
            returnValue.Add(new Models.Domain() { Name = "MapQuest", Weight = 0 });
            returnValue.Add(new Models.Domain() { Name = "MerchantCircle", Weight = 0 });
            returnValue.Add(new Models.Domain() { Name = "TripAdvisor", Weight = 0 });
            returnValue.Add(new Models.Domain() { Name = "YahooLocal", Weight = 0 });
            returnValue.Add(new Models.Domain() { Name = "YellowPages", Weight = 0 });
            returnValue.Add(new Models.Domain() { Name = "Yelp", Weight = 0 });
            returnValue.Add(new Models.Domain() { Name = "HealthGrades", Weight = 0 });
            returnValue.Add(new Models.Domain() { Name = "MDcom", Weight = 0 });

            return returnValue;
        }

        public Models.DataTypesAPI.Application GetApplication()
        {
            return new Models.DataTypesAPI.Application()
            {
                _id = "587fa08eff4c4e0904208a2a",
                Description = "Location Data Health",
                Name = GetApplicationName(),
                Schemas = new List<Models.DataTypesAPI.Schema>()
                {
                    new Models.DataTypesAPI.Schema()
                    {
                        ComponentName = GetComponentName(),
                        Jschema = "{\r\n  \"type\": \"object\",\r\n  \"properties\": {\r\n    \"Domains\": {\r\n      \"required\": true,\r\n      \"type\": [\r\n        \"array\",\r\n        \"null\"\r\n      ],\r\n      \"items\": {\r\n        \"type\": [\r\n          \"object\",\r\n          \"null\"\r\n        ],\r\n        \"properties\": {\r\n          \"Name\": {\r\n            \"required\": true,\r\n            \"type\": [\r\n              \"string\",\r\n              \"null\"\r\n            ]\r\n          },\r\n          \"Weight\": {\r\n            \"required\": true,\r\n            \"type\": \"number\"\r\n          }\r\n        }\r\n      }\r\n    }\r\n  }\r\n}"
                    }
                }
            };
        }

        public string GetComponentName()
        {
            return ConfigurationManager.AppSettings["BusinessTypesSettingsComponentName"];
        }

        public string GetApplicationName()
        {
            return ConfigurationManager.AppSettings["BusinessTypesSettingsApplicationName"];
        }

        public string GetBusinessTypeSettingsName(string BusinessTypeName)
        {
            return string.Format("{0} Settings", BusinessTypeName);
        }
    }
}