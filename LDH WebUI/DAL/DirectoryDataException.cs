﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH.Management.WebUI.DAL
{
    public class DirectoryDataException : Exception
    {
        public DirectoryDataException()
        {

        }

        public DirectoryDataException(Exception InnerException) : base(InnerException.Message, InnerException)
        {
        }

        public DirectoryDataException(string Message, Exception InnerException) : base(Message, InnerException)
        {

        }
    }
}