﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

using MySql.Data;
using MySql.Data.MySqlClient;

using LDH.Management.WebUI.Models;
using LDH.Management.WebUI.Models.Enums;

namespace LDH.Management.WebUI.DAL
{
    public class Process
    {
        public int CreateProcess()
        {
            var result = 0;
            var conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["LDH_MySQL"].ConnectionString);
            var cmd = new MySqlCommand("add_Process", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "prc_id", DbType = DbType.Int32, Direction = ParameterDirection.Output });

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                result = (int)cmd.Parameters["prc_id"].Value;
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }

            return result;
        }

        public int AddTask(int ProcessID, int DirectoryID, int SycaraAccountID)
        {
            var result = 0;
            var conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["LDH_MySQL"].ConnectionString);
            var cmd = new MySqlCommand("add_Task", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "prc_id", DbType = DbType.Int32, Direction = ParameterDirection.Input, Value = ProcessID });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "dir_id", DbType = DbType.Int32, Direction = ParameterDirection.Input, Value = DirectoryID });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "sycara_account_id", DbType = DbType.Int32, Direction = ParameterDirection.Input, Value = SycaraAccountID });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "task_id", DbType = DbType.Int32, Direction = ParameterDirection.Output });

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                result = (int)cmd.Parameters["task_id"].Value;
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }

            return result;
        }
    }
}