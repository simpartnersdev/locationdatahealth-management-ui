﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LDH.Management.WebUI.Startup))]
namespace LDH.Management.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
