﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDH.Management.BTSettings.WorkerService.BL
{
    public class BusinessType
    {
        public void CalculateBusinessTypeAltScore(object sender, System.Timers.ElapsedEventArgs e)
        {
            var runner = (System.Timers.Timer)sender;

            try
            {
                //stop the timer, so it won't raise a second thread doing the same thing before this thread completes its job
                runner.Stop();

                var btManager = new DAL.BusinessType();
                var StepToRun = btManager.GetWorkerStep();
                var BusinessTypeData = new DAL.BusinessType().GetBusinessTypeData();
                var TotalSteps = 5; //to account for the first and last steps (2 steps at the beginning and 3 at the end)
                var CurrentStep = 0;

                foreach (var bt in BusinessTypeData)
                {
                    foreach (var dom in bt.Domains)
                    {
                        TotalSteps++;
                    }
                }

                if (StepToRun != Enums.SettingsWorkerStep.Idle)
                {
                    DAL.Log.AddMessage(string.Format("Found a job to run: {0}", StepToRun.ToString()));
                }

                if (StepToRun == Enums.SettingsWorkerStep.ResetScore)
                {
                    CurrentStep++;

                    //1. Reset scores
                    btManager.SetWorkerStep(Enums.SettingsWorkerStep.ResetScore, GetNormalizedProgress(CurrentStep, TotalSteps));
                    btManager.ResetAltDomainScores();
                    DAL.Log.AddMessage(string.Format("Finished Resetting Domain Scores, Progress: {1}", StepToRun.ToString(), GetNormalizedProgress(CurrentStep, TotalSteps)));
                    btManager.ResetAltProfileScores();
                    DAL.Log.AddMessage(string.Format("Finished Resetting Profile Scores, Progress: {1}", StepToRun.ToString(), GetNormalizedProgress(CurrentStep, TotalSteps)));

                    CurrentStep++;

                    StepToRun = btManager.SetWorkerStep(Enums.SettingsWorkerStep.UpdateDomainWeights, GetNormalizedProgress(CurrentStep, TotalSteps));
                    DAL.Log.AddMessage(string.Format("Moving to next step: {0}, Progress: {1}", StepToRun.ToString(), GetNormalizedProgress(CurrentStep, TotalSteps)));
                }
                else
                {
                    CurrentStep++;
                    CurrentStep++;
                }

                if (StepToRun == Enums.SettingsWorkerStep.UpdateDomainWeights)
                {
                    //2. Update weights
                    foreach (var bt in BusinessTypeData)
                    {
                        foreach (var dom in bt.Domains)
                        {
                            btManager.AssignAltDomainWeight(dom.Name, dom.Weight, bt.Business);
                            CurrentStep++;

                            btManager.SetWorkerStep(Enums.SettingsWorkerStep.UpdateDomainWeights, GetNormalizedProgress(CurrentStep, TotalSteps));
                            DAL.Log.AddMessage(string.Format("Working: {0}, Progress: {1}", StepToRun.ToString(), GetNormalizedProgress(CurrentStep, TotalSteps)));
                        }
                    }

                    StepToRun = btManager.SetWorkerStep(Enums.SettingsWorkerStep.CalculateAltScores, GetNormalizedProgress(CurrentStep, TotalSteps));
                    DAL.Log.AddMessage(string.Format("Moving to next step: {0}, Progress: {1}", StepToRun.ToString(), GetNormalizedProgress(CurrentStep, TotalSteps)));
                }
                else
                {
                    foreach (var bt in BusinessTypeData)
                    {
                        foreach (var dom in bt.Domains)
                        {
                            CurrentStep++;
                        }
                    }
                }

                if (StepToRun == Enums.SettingsWorkerStep.CalculateAltScores)
                {
                    //3. Recalc scores
                    btManager.CalculateAltDomainScore();
                    CurrentStep++;
                    btManager.SetWorkerStep(Enums.SettingsWorkerStep.CalculateAltScores, GetNormalizedProgress(CurrentStep, TotalSteps));

                    btManager.CalculateAltProfileScore();
                    CurrentStep++;
                    btManager.SetWorkerStep(Enums.SettingsWorkerStep.CalculateAltScores, GetNormalizedProgress(CurrentStep, TotalSteps));

                    DAL.Log.AddMessage(string.Format("Finished Calculating ALT Scores, Progress: {1}", StepToRun.ToString(), GetNormalizedProgress(CurrentStep, TotalSteps)));

                    btManager.CalculateAltDomainScoreDelta();
                    CurrentStep++;
                    btManager.SetWorkerStep(Enums.SettingsWorkerStep.CalculateAltScores, GetNormalizedProgress(CurrentStep, TotalSteps));

                    btManager.CalculateAltProfileScoreDelta();
                    btManager.SetWorkerStep(Enums.SettingsWorkerStep.CalculateAltScores, 100);
                    DAL.Log.AddMessage(string.Format("Finished Calculating ALT Score Deltas, Progress: {1}", StepToRun.ToString(), 100));

                    //this is just to give the UI a chance to show the 100% value. It will be set to 0% again after this.
                    System.Threading.Thread.Sleep(30000);

                    StepToRun = btManager.SetWorkerStep(Enums.SettingsWorkerStep.Idle, 0);
                    DAL.Log.AddMessage(string.Format("Work Completed! Setting next step: {0}, Progress: {1}", StepToRun.ToString(), 0));
                }
            }
            catch (Exception ex)
            {
                DAL.Log.AddMessage(string.Format("An error occurred while updating the BusinessType ALT Scores. Error: {0}.\r\nStack Trace: {1}", ex.Message, ex.StackTrace));
            }
            finally
            {
                //now restart the timer
                runner.Start();
            }
        }

        //Normal values for progress while woring will range between 1 and 99. The value of 0 and 100 are manually set.
        private int GetNormalizedProgress(int CurrentStep, int TotalSteps)
        {
            var Progress = (int)((decimal)CurrentStep * 100 / (decimal)TotalSteps);
            Progress = Progress <= 0 ? 1 : Progress;
            Progress = Progress >= 100 ? 99 : Progress;
            return Progress;
        }
    }
}
