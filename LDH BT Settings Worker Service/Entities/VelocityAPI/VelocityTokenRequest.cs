﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH.Management.BTSettings.WorkerService.Entities.VelocityAPI
{
    internal class VelocityTokenRequest
    {
        public string ApiKey { get; set; }
        public int DirectoryID { get; set; }
        public int UserID { get; set; }
    }
}