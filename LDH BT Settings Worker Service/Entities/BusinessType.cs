﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LDH.Management.BTSettings.WorkerService.Entities
{
    public class BusinessType
    {
        [Key]
        public string Business { get; set; }
        public List<Domain> Domains { get; set; }
    }
}
