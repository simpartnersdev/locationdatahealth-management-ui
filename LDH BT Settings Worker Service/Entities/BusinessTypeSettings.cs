﻿using System;
using System.Collections.Generic;

namespace LDH.Management.BTSettings.WorkerService.Entities
{
    public class BusinessTypesServiceResponse
    {
        public List<BusinessTypeSettings> Settings { get; set; }
    }

    public class BusinessTypeSettings
    {
        public string BusinessType { get; set; }
        public string Settings { get; set; }
    }

    public class BusinessTypesDomains
    {
        public List<Domain> Domains;
    }
}
