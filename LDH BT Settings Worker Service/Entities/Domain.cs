﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LDH.Management.BTSettings.WorkerService.Entities
{
    public class Domain
    {
        [Key]
        public string Name { get; set; }
        public decimal Weight { get; set; }
    }
}
