﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace LDH.Management.BTSettings.WorkerService
{
    public partial class BTSettingsWorkerService : ServiceBase
    {
        private Timer settings = null;
        private BL.BusinessType settings_worker = null;

        public BTSettingsWorkerService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var seedInterval = Convert.ToDouble(ConfigurationManager.AppSettings["SettingsUpdateInterval"]);
            settings_worker = new BL.BusinessType();

            settings = new Timer(seedInterval);
            settings.AutoReset = false;
            settings.Elapsed += new ElapsedEventHandler(settings_worker.CalculateBusinessTypeAltScore);
            settings.Start();

            DAL.Log.AddMessage("LDH BT Settings Worker Service started.");
        }

        protected override void OnStop()
        {
            settings_worker = null;
            settings.Enabled = false;

            DAL.Log.AddMessage("LDH BT Settings Worker Service stopped.");
        }
    }
}
