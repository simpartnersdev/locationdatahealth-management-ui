﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.IO;
using System.Net;
using System.Text;

using Newtonsoft.Json;

using System.Data.SqlClient;

namespace LDH.Management.BTSettings.WorkerService.DAL
{
    class BusinessType
    {
        public List<Entities.BusinessType> GetBusinessTypeData()
        {
            var data = string.Empty;
            var request = (HttpWebRequest)HttpWebRequest.Create(ConfigurationManager.AppSettings["BusinessTypesSettingsService"]);
            request.Headers.Add("Token", VelocityToken.Token);
            request.Method = "GET";

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                TextReader tr = new StreamReader(response.GetResponseStream());
                data = tr.ReadToEnd();
            }

            //var resp = JsonConvert.DeserializeObject<Models.BusinessTypesServiceResponse>(data);
            //return resp.Settings;

            var BTServiceResponse = JsonConvert.DeserializeObject<Entities.BusinessTypesServiceResponse>(data);
            var BusinessTypes = new List<Entities.BusinessType>();
            var tmpBT = new Entities.BusinessType();

            foreach (var BT in BTServiceResponse.Settings)
            {
                tmpBT = new Entities.BusinessType()
                {
                    Business = BT.BusinessType,
                    Domains = JsonConvert.DeserializeObject<Entities.BusinessTypesDomains>(BT.Settings).Domains
                };
                //TODO: remove this nasty fix once the BT service fixes the names of the domains
                for (int i = 0; i < tmpBT.Domains.Count; i++)
                {
                    tmpBT.Domains[i].Name = tmpBT.Domains[i].Name.Replace(" ", string.Empty);
                }

                BusinessTypes.Add(tmpBT);
            }
            return BusinessTypes;
        }

        public void ResetAltDomainScores()
        {
            var dtListings = new DataTable();
            var dtProfilesSummary = new DataTable();

            var sbQuery = new StringBuilder();
            sbQuery.AppendLine("UPDATE");
            sbQuery.AppendLine("	ldh.lis_listing_score");
            sbQuery.AppendLine("SET");
            sbQuery.AppendLine("	lis_alternative_score = NULL,");
            sbQuery.AppendLine("	lis_alternative_score_delta = NULL,");
            sbQuery.AppendLine("	lis_alternative_domain_weight = 0.0;");

            OdbcConnection oCon = new OdbcConnection(Utils.GetRedshiftConnectionString());
            OdbcCommand oCmd = new OdbcCommand(sbQuery.ToString(), oCon);

            try
            {
                oCon.Open();
                oCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (oCon.State != ConnectionState.Closed)
                    oCon.Close();
            }
        }

        public void ResetAltProfileScores()
        {
            var dtListings = new DataTable();
            var dtProfilesSummary = new DataTable();

            var sbQuery = new StringBuilder();
            sbQuery.AppendLine("UPDATE");
            sbQuery.AppendLine("	ldh.prs_profile_score");
            sbQuery.AppendLine("SET");
            sbQuery.AppendLine("	prs_alternative_score = NULL,");
            sbQuery.AppendLine("	prs_alternative_score_delta = NULL");

            OdbcConnection oCon = new OdbcConnection(Utils.GetRedshiftConnectionString());
            OdbcCommand oCmd = new OdbcCommand(sbQuery.ToString(), oCon);

            try
            {
                oCon.Open();
                oCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (oCon.State != ConnectionState.Closed)
                    oCon.Close();
            }
        }

        public void AssignAltDomainWeight(string Domain, decimal Weight, string BusinessType)
        {
            var dtListings = new DataTable();
            var dtProfilesSummary = new DataTable();

            var sbQuery = new StringBuilder();
            sbQuery.AppendLine("UPDATE");
            sbQuery.AppendLine("    ldh.lis_listing_score");
            sbQuery.AppendLine("SET");
            sbQuery.AppendLine(string.Format("    lis_alternative_domain_weight = {0}", Weight));
            sbQuery.AppendLine("FROM");
            sbQuery.AppendLine("    ldh.pro_profile P");
            sbQuery.AppendLine("WHERE");
            sbQuery.AppendLine("    lis_pro_id = P.pro_id");
            sbQuery.AppendLine(string.Format("    AND lis_dom_id = '{0}'", Domain));
            sbQuery.AppendLine(string.Format("    AND P.pro_business_type = '{0}';", BusinessType));

            OdbcConnection oCon = new OdbcConnection(Utils.GetRedshiftConnectionString());
            OdbcCommand oCmd = new OdbcCommand(sbQuery.ToString(), oCon);

            try
            {
                oCon.Open();
                oCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (oCon.State != ConnectionState.Closed)
                    oCon.Close();
            }
        }

        public void CalculateAltDomainScore()
        {
            var dtListings = new DataTable();
            var dtProfilesSummary = new DataTable();

            var sbQuery = new StringBuilder();
            sbQuery.AppendLine("UPDATE");
            sbQuery.AppendLine("    ldh.lis_listing_score");
            sbQuery.AppendLine("SET");
            sbQuery.AppendLine("    lis_alternative_score = lis_correctness * lis_alternative_domain_weight / 100");

            OdbcConnection oCon = new OdbcConnection(Utils.GetRedshiftConnectionString());
            OdbcCommand oCmd = new OdbcCommand(sbQuery.ToString(), oCon);

            try
            {
                oCon.Open();
                oCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (oCon.State != ConnectionState.Closed)
                    oCon.Close();
            }
        }

        public void CalculateAltDomainScoreDelta()
        {
            var dtListings = new DataTable();
            var dtProfilesSummary = new DataTable();

            var sbQuery = new StringBuilder();
            sbQuery.AppendLine("UPDATE");
            sbQuery.AppendLine("	ldh.lis_listing_score");
            sbQuery.AppendLine("SET");
            sbQuery.AppendLine("	lis_alternative_score_delta = lis_alternative_score - ( SELECT");
            sbQuery.AppendLine("																lis_alternative_score");
            sbQuery.AppendLine("															FROM");
            sbQuery.AppendLine("																ldh.lis_listing_score L");
            sbQuery.AppendLine("															WHERE");
            sbQuery.AppendLine("																L.lis_pro_id = lis_listing_score.lis_pro_id");
            sbQuery.AppendLine("																AND L.lis_dom_id = lis_listing_score.lis_dom_id");
            sbQuery.AppendLine("																AND TO_CHAR(L.lis_tim_date, 'MM/YYYY') = TO_CHAR(ADD_MONTHS(lis_listing_score.lis_tim_date, -1), 'MM/YYYY'));");


            OdbcConnection oCon = new OdbcConnection(Utils.GetRedshiftConnectionString());
            OdbcCommand oCmd = new OdbcCommand(sbQuery.ToString(), oCon);

            try
            {
                oCon.Open();
                oCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (oCon.State != ConnectionState.Closed)
                    oCon.Close();
            }
        }

        public void CalculateAltProfileScoreDelta()
        {
            var dtListings = new DataTable();
            var dtProfilesSummary = new DataTable();

            var sbQuery = new StringBuilder();
            sbQuery.AppendLine("UPDATE");
            sbQuery.AppendLine("	ldh.prs_profile_score");
            sbQuery.AppendLine("SET");
            sbQuery.AppendLine("	prs_alternative_score_delta = prs_score - (	SELECT");
            sbQuery.AppendLine("													PS.prs_alternative_score");
            sbQuery.AppendLine("												FROM");
            sbQuery.AppendLine("													ldh.prs_profile_score PS");
            sbQuery.AppendLine("												WHERE");
            sbQuery.AppendLine("													PS.prs_pro_id = prs_profile_score.prs_pro_id");
            sbQuery.AppendLine("													AND TO_CHAR(PS.prs_tim_date, 'MM/YYYY') = TO_CHAR(ADD_MONTHS(prs_profile_score.prs_tim_date, -1), 'MM/YYYY'));");


            OdbcConnection oCon = new OdbcConnection(Utils.GetRedshiftConnectionString());
            OdbcCommand oCmd = new OdbcCommand(sbQuery.ToString(), oCon);

            try
            {
                oCon.Open();
                oCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (oCon.State != ConnectionState.Closed)
                    oCon.Close();
            }
        }

        public void CalculateAltProfileScore()
        {
            var dtListings = new DataTable();
            var dtProfilesSummary = new DataTable();

            var sbQuery = new StringBuilder();
            sbQuery.AppendLine("UPDATE");
            sbQuery.AppendLine("	ldh.prs_profile_score");
            sbQuery.AppendLine("SET");
            sbQuery.AppendLine("	prs_alternative_score = ALT.alternative_score");
            sbQuery.AppendLine("FROM");
            sbQuery.AppendLine("	(");
            sbQuery.AppendLine("	SELECT");
            sbQuery.AppendLine("		lis_pro_id,");
            sbQuery.AppendLine("		TO_CHAR(lis_tim_date, 'YYYYMM') lis_month,");
            sbQuery.AppendLine("		SUM(lis_alternative_score) alternative_score");
            sbQuery.AppendLine("	FROM");
            sbQuery.AppendLine("		ldh.lis_listing_score");
            sbQuery.AppendLine("	GROUP BY");
            sbQuery.AppendLine("		lis_pro_id,");
            sbQuery.AppendLine("		TO_CHAR(lis_tim_date, 'YYYYMM')");
            sbQuery.AppendLine("	) ALT");
            sbQuery.AppendLine("WHERE");
            sbQuery.AppendLine("	prs_profile_score.prs_pro_id = ALT.lis_pro_id");
            sbQuery.AppendLine("	AND TO_CHAR(prs_profile_score.prs_tim_date, 'YYYYMM') = lis_month");


            OdbcConnection oCon = new OdbcConnection(Utils.GetRedshiftConnectionString());
            OdbcCommand oCmd = new OdbcCommand(sbQuery.ToString(), oCon);

            try
            {
                oCon.Open();
                oCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (oCon.State != ConnectionState.Closed)
                    oCon.Close();
            }
        }

        public Enums.SettingsWorkerStep GetWorkerStep()
        {
            var returnValue = Enums.SettingsWorkerStep.Idle;
            var oCon = new SqlConnection(ConfigurationManager.ConnectionStrings["LDH_SQL"].ConnectionString);
            var oCmd = new SqlCommand("bts.bts_GetCurrentStep", oCon);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add(new SqlParameter() { ParameterName = "@CurrentStep", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Output });
            try
            {
                oCon.Open();
                oCmd.ExecuteNonQuery();

                returnValue = (Enums.SettingsWorkerStep)(int)oCmd.Parameters["@CurrentStep"].Value;
            }
            finally
            {
                if (oCon.State != ConnectionState.Closed)
                {
                    oCon.Close();
                }
            }

            return returnValue;
        }

        public Enums.SettingsWorkerStep SetWorkerStep(Enums.SettingsWorkerStep Step, int Progress)
        {
            var oCon = new SqlConnection(ConfigurationManager.ConnectionStrings["LDH_SQL"].ConnectionString);
            var oCmd = new SqlCommand("bts.bts_SetCurrentStep", oCon);
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.Parameters.Add(new SqlParameter() { ParameterName = "@CurrentStep", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = (int)Step });
            oCmd.Parameters.Add(new SqlParameter() { ParameterName = "@Progress", SqlDbType = SqlDbType.Int, Direction = ParameterDirection.Input, Value = Progress });
            try
            {
                oCon.Open();
                oCmd.ExecuteNonQuery();
            }
            finally
            {
                if (oCon.State != ConnectionState.Closed)
                {
                    oCon.Close();
                }
            }

            return Step;
        }
    }
}
