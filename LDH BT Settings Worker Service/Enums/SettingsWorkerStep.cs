﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDH.Management.BTSettings.WorkerService.Enums
{
    public enum SettingsWorkerStep
    {
        Idle = 0,
        ResetScore = 1,
        UpdateDomainWeights = 2,
        CalculateAltScores = 3
    }
}
