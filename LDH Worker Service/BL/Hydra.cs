﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace LDH.Management.WorkerService.BL
{
    public class Hydra
    {
        public void LaunchParsingJobs(Entities.Task LDHTask)
        {
            //var Directories = new DAL.Directory().GetDirectories(Enums.DirectoryStatus.Active);
            var ProfileData = new List<Entities.HydraJob>();
            var taskManager = new DAL.Task();
            var hydraAPI = new DAL.HydraRESTApiV2();
            var textResponse = string.Empty;
            var ReportID = 0;
            var JobID = 0;
            var HydraPayload = string.Empty;
            //tough luck, Hydra's response changes shape, so deserialization is not straightofrward
            //I'll be using dynamic "type" until I can figure out a better way to handle the response from Hydra.
            dynamic HydraResponse;

            ProfileData = new DAL.Directory().GetProfileJobData(LDHTask.DirectoryID);
            
            foreach (var profile in ProfileData)
            {
                try
                {
                    //create the job first, the unique index in the jobs table will help us make sure we're not duplicating workload into Hydra
                    JobID = taskManager.CreateJob(LDHTask.ID, profile.ID, profile.SycaraBusinessID, profile.BusinessType);

                    //schedule the job in Hydra
                    HydraResponse = JsonConvert.DeserializeObject(hydraAPI.CreateDirectoryListingJob(profile, DAL.BusinessType.GetBusinessType(string.IsNullOrEmpty(profile.BusinessType) ? profile.DirectoryBusinessType : profile.BusinessType).Domains, out HydraPayload));

                    if (HydraResponse.code == 200)
                    {
                        //got a good response from Hydra!
                        ReportID = HydraResponse.response.report_id;
                        //now that we have a report id, let's set the job as active
                        taskManager.ActivateJob(JobID, ReportID, profile);
                    }
                    else
                    {
                        //TODO: most likely an error, need to work it out later
                        DAL.Log.AddMessage(string.Format("Unexpected response from Hydra while creating new Job. Process ID: {0}. Task ID: {1}. Directory ID: {2}. Profile ID: {3}. Business Type: {4}.\r\nHydra Response: {5}\r\nHydra Payload: {6}", LDHTask.ProcessID, LDHTask.ID, LDHTask.DirectoryID, profile.ID, profile.BusinessType, HydraResponse, HydraPayload));
                    }

                    //reset values so logs can reflect the proper current status in case something goes wrong.
                    HydraPayload = string.Empty;
                    HydraResponse = string.Empty;
                    ReportID = 0;
                }
                catch (Exception ex)
                {
                    DAL.Log.AddMessage(string.Format("Failed to create new Job. Process ID: {0}. Task ID: {1}. Report ID: {2}. Directory ID: {3}. Profile ID: {4}. Business Type: {5}. Error: {6}", LDHTask.ProcessID, LDHTask.ID, ReportID, LDHTask.DirectoryID, profile.ID, profile.BusinessType, ex.Message));
                }
            }
        }

        public string GetReport(Entities.Job Job)
        {
            var returnValue = string.Empty;
            var hydraAPI = new DAL.HydraRESTApiV2();
            var JobManager = new DAL.Job();
            var parsedData = string.Empty;
            //Hydra's response changes shape, so deserialization is not straightofrward
            //I'll be using dynamic "type" until I can figure out a better way to handle the response from Hydra.
            dynamic HydraResponse;

            HydraResponse = JsonConvert.DeserializeObject(hydraAPI.GetReport(Job));

            if (HydraResponse.code == 200)
            {
                //Report completed, good to go
                parsedData = HydraResponse.response.ToString();
                returnValue = parsedData;
                if (parsedData.Length > 5)
                {
                    //damned Hydra Response not only changes shape, it also uses randomly named objects instead of an array of objects >:(
                    //need to manipulate the json to convert into an array so the objects can deserialize properly
                    parsedData = string.Format("[{0}]", parsedData.Substring(1, parsedData.Length - 2));
                    parsedData = Regex.Replace(parsedData, @"\""\d+_\d+_\w+\"":\s", "");
                    var HydraReport = new Entities.HydraReport() { ParsedData = JsonConvert.DeserializeObject<List<Entities.DomainData>>(parsedData) };

                    foreach (var domData in HydraReport.ParsedData)
                    {
                        //calculate health score data and store in db
                        JobManager.AddListingAccuracyData(new Entities.AccuracyData(Job, domData));
                    }
                }
                else
                {
                    //most likely an error, must log and continue
                    DAL.Log.AddMessage(string.Format("Invalid content for Hydra report (too short). Task ID: {0}. Job ID: {1}. Report ID: {2}. Profile ID: {3}.\r\nHydra Response: {4}", Job.TaskID, Job.ID, Job.ReportID, Job.ProfileID, HydraResponse));
                }
                
            }
            else if (HydraResponse.code == 202)
            {
                //Report not ready yet... do nothing, chill out, have a cup of coffee :)
            }
            else
            {
                //most likely an error, need to work it out later
                DAL.Log.AddMessage(string.Format("Unexpected response from Hydra while retrieving Job's report. Task ID: {0}. Job ID: {1}. Report ID: {2}. Profile ID: {3}.\r\nHydra Response: {4}", Job.TaskID, Job.ID, Job.ReportID, Job.ProfileID, HydraResponse));
            }
            return returnValue;
        }
    }
}