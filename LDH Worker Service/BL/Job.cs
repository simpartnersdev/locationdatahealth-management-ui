﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDH.Management.WorkerService.BL
{
    public class Job
    {
        public void CollectCompletedJobs(object sender, System.Timers.ElapsedEventArgs e)
        {
            var runner = (System.Timers.Timer)sender;

            try
            {
                //stop the timer, so it won't raise a second thread doing the same thing before this thread completes its job
                runner.Stop();

                var jobManager = new DAL.Job();
                var Jobs = jobManager.GetActiveJobs();
                var hydra = new Hydra();
                var jsonData = string.Empty;

                //always make sure to refresh the local copy of the business types before processing the jobs
                DAL.BusinessType.RefreshBusinessTypes();

                foreach (var job in Jobs)
                {
                    try
                    {
                        jsonData = hydra.GetReport(job);
                        if (jsonData.Length > 0)
                        {
                            jobManager.CompleteActiveJob(job.ID, jsonData);
                        }
                    }
                    catch (Exception ex)
                    {
                        DAL.Log.AddMessage(string.Format("An unexpected error occurred while completing active jobs. Error: {0}\r\nStack Trace: {1}", ex.Message, ex.StackTrace));
                    }
                }
            }
            catch (Exception ex)
            {
                DAL.Log.AddMessage(string.Format("An unexpected error occurred while collecting completed jobs. Error: {0}\r\nStack Trace: {1}", ex.Message, ex.StackTrace));
            }
            finally
            {
                //now restart the timer
                runner.Start();
            }
        }
    }
}
