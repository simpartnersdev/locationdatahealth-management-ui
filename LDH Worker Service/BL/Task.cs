﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDH.Management.WorkerService.BL
{
    public class Task
    {
        public void ProcessPendingTasks(object sender, System.Timers.ElapsedEventArgs e)
        {
            var runner = (System.Timers.Timer)sender;

            try
            {
                //stop the timer, so it won't raise a second thread doing the same thing before this thread completes its job
                runner.Stop();

                var taskManager = new DAL.Task();
                var Tasks = taskManager.GetPendingTasks();
                var hydra = new Hydra();

                //always make sure to refresh the local copy of the business types before processing the tasks
                DAL.BusinessType.RefreshBusinessTypes();

                foreach (var task in Tasks)
                {
                    try
                    {
                        hydra.LaunchParsingJobs(task);
                        taskManager.SetQueuedTask(task.ID);
                    }
                    catch (Exception ex)
                    {
                        DAL.Log.AddMessage(string.Format("An unexpected error occurred while launching jobs. Error: {0}\r\nStack Trace: {1}", ex.Message, ex.StackTrace));
                    }
                    
                }
            }
            catch (Exception ex)
            {
                DAL.Log.AddMessage(string.Format("An unexpected error occurred while processing pending tasks. Error: {0}", ex.Message));
            }
            finally
            {
                //now restart the timer
                runner.Start();
            }
        }
    }
}
