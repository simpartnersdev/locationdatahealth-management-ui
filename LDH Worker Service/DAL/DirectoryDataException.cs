﻿using System;

namespace LDH.Management.WorkerService.DAL
{
    public class DirectoryDataException : Exception
    {
        public DirectoryDataException()
        {

        }

        public DirectoryDataException(Exception InnerException) : base(InnerException.Message, InnerException)
        {
        }

        public DirectoryDataException(string Message, Exception InnerException) : base(Message, InnerException)
        {

        }
    }
}