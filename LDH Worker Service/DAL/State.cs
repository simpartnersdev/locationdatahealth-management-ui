﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDH.Management.WorkerService.DAL
{
    public static class State
    {
        private static Dictionary<string, string> StateList = new Dictionary<string, string>
        {
            {"al", "alabama"},
            {"ak", "alaska"},
            {"az", "arizona"},
            {"ar", "arkansas"},
            {"ca", "california"},
            {"co", "colorado"},
            {"ct", "connecticut"},
            {"de", "delaware"},
            {"dc", "washington dc"},
            {"fl", "florida"},
            {"ga", "georgia"},
            {"hi", "hawaii"},
            {"id", "idaho"},
            {"il", "illinois"},
            {"in", "indiana"},
            {"ia", "iowa"},
            {"ks", "kansas"},
            {"ky", "kentucky"},
            {"la", "louisiana"},
            {"me", "maine"},
            {"md", "maryland"},
            {"ma", "massachusetts"},
            {"mi", "michigan"},
            {"mn", "minnesota"},
            {"ms", "mississippi"},
            {"mo", "missouri"},
            {"mt", "montana"},
            {"ne", "nebraska"},
            {"nv", "nevada"},
            {"nh", "new hampshire"},
            {"nj", "new jersey"},
            {"nm", "new mexico"},
            {"ny", "new york"},
            {"nc", "north carolina"},
            {"nd", "north dakota"},
            {"oh", "ohio"},
            {"ok", "oklahoma"},
            {"or", "oregon"},
            {"pa", "pennsylvania"},
            {"ri", "rhode island"},
            {"sc", "south carolina"},
            {"sd", "south dakota"},
            {"tn", "tennessee"},
            {"tx", "texas"},
            {"ut", "utah"},
            {"vt", "vermont"},
            {"va", "virginia"},
            {"vi", "virgin islands"},
            {"wa", "washington"},
            {"wv", "west virginia"},
            {"wi", "wisconsin"},
            {"wy", "wyoming"}
        };

        public static string GetState(string State)
        {
            var returnValue = string.Empty;

            if (!StateList.TryGetValue(State.ToLower(), out returnValue))
            {
                if (StateList.Count(x => x.Value == State.ToLower()) > 0)
                {
                    returnValue = StateList.First(x => x.Value == State.ToLower()).Value;
                }
            }

            if (returnValue == null)
                returnValue = string.Empty;

            return returnValue;
        }
    }
}
