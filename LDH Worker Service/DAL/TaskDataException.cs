﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDH.Management.WorkerService.DAL
{
    class TaskDataException : Exception
    {
        public TaskDataException()
        {

        }

        public TaskDataException(Exception InnerException) : base(InnerException.Message, InnerException)
        {
        }

        public TaskDataException(string Message, Exception InnerException) : base(Message, InnerException)
        {

        }
    }
}
