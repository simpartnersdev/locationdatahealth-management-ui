﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;

using Newtonsoft.Json;

namespace LDH.Management.WorkerService.DAL
{
    public static class VelocityToken
    {
        private static string _velocityToken;
        private static DateTime _tokenLastUpdated;

        public static string Token
        {
            get
            {
                RefreshVelocityToken();
                return _velocityToken;
            }
        }

        private static void RefreshVelocityToken()
        {
            try
            {
                if (string.IsNullOrEmpty(_velocityToken) || (DateTime.Now - _tokenLastUpdated).TotalMinutes >= int.Parse(ConfigurationManager.AppSettings["VelocityTokenTimeout"]))
                {
                    var Payload = JsonConvert.SerializeObject(new Entities.VelocityAPI.VelocityTokenRequest() { ApiKey = ConfigurationManager.AppSettings["VelocityAPIKey"] });
                    var data = string.Empty;
                    var request = (HttpWebRequest)HttpWebRequest.Create(ConfigurationManager.AppSettings["VelocityTokenAPI"]);
                    request.Method = "POST";
                    request.ContentType = "application/json";

                    using (var sw = new StreamWriter(request.GetRequestStream()))
                    {
                        sw.Write(Payload);
                        sw.Flush();
                        sw.Close();
                    }

                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        TextReader tr = new StreamReader(response.GetResponseStream());
                        data = tr.ReadToEnd();
                    }

                    var VelocityAPIResponse = JsonConvert.DeserializeObject<Entities.VelocityAPI.VelocityTokenResponse>(data);

                    if (VelocityAPIResponse.Success)
                    {
                        _velocityToken = VelocityAPIResponse.Token;
                        _tokenLastUpdated = DateTime.Now;
                    }
                    else
                    {
                        DAL.Log.AddMessage(string.Format("Unable to get a token from Velocity API. Error: {0}", VelocityAPIResponse.Error));
                    }
                }
            }
            catch (Exception ex)
            {
                DAL.Log.AddMessage(string.Format("Unable to get a token from Velocity API. Error: {0}", ex.Message));
            }
        }
    }
}