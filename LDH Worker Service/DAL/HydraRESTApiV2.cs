﻿using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using Newtonsoft.Json;

using LDH.Management.WorkerService.Entities;

namespace LDH.Management.WorkerService.DAL
{
    public class HydraRESTApiV2
    {
        public string CreateDirectoryListingJob(HydraJob BaseJobInfo, List<Domain> Domains, out string Payload)
        {
            Payload = string.Empty;
            var resp = string.Empty;
            var DirectoryListings = new DirectoryListingRequest() { Apikey = BaseJobInfo.ApiKey, ListingRequests = new List<HydraJob>() };

            //TODO: Remove this nasty fix after we're done re-parsing March 2017
            foreach (var domain in Domains.FindAll(x => DomainIsEnabled(x.Name)))
            {
                DirectoryListings.ListingRequests.Add(new HydraJob()
                {
                    Address = BaseJobInfo.Address,
                    City = BaseJobInfo.City,
                    Country = BaseJobInfo.Country,
                    Engine = domain.Name,
                    ID = BaseJobInfo.ID,
                    Language = BaseJobInfo.Language,
                    Name = BaseJobInfo.Name,
                    Phone = BaseJobInfo.Phone,
                    State = BaseJobInfo.State,
                    ZipCode = BaseJobInfo.ZipCode
                });
            }

            Payload = JsonConvert.SerializeObject(DirectoryListings);

            var request = (HttpWebRequest)HttpWebRequest.Create(ConfigurationManager.AppSettings["HydraAPI_V2_directory_listings"]);
            request.Method = "POST";
            request.ContentType = "application/json";

            using (var sw = new StreamWriter(request.GetRequestStream()))
            {
                sw.Write(Payload);
                sw.Flush();
                sw.Close();
            }

            request.BeginGetResponse((x) =>
            {
                using (HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(x))
                {
                    if (response.ContentEncoding == "gzip")
                    {
                        resp = Utils.gzUncompressText(response.GetResponseStream());
                    }
                    else
                    {
                        TextReader tr = new StreamReader(response.GetResponseStream());
                        resp = tr.ReadToEnd();
                    }
                }
            }, null);

            return resp;
        }

        private bool DomainIsEnabled(string Domain)
        {
            if (ConfigurationManager.AppSettings["EnabledDomains"].Trim() == "*")
            {
                return true;
            }

            var EnabledDomains = new List<string>(ConfigurationManager.AppSettings["EnabledDomains"].Split(','));
            if (EnabledDomains.Contains(Domain))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string GetReport(Entities.Job Job)
        {
            var resp = string.Empty;
            var request = (HttpWebRequest)HttpWebRequest.Create(string.Format("{0}/{1}?api_key={2}",ConfigurationManager.AppSettings["HydraAPI_V2_directory_listings"], Job.ReportID, Job.ApiKey));
            request.Method = "GET";

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.ContentEncoding == "gzip")
                {
                    resp = Utils.gzUncompressText(response.GetResponseStream());
                }
                else
                {
                    TextReader tr = new StreamReader(response.GetResponseStream());
                    resp = tr.ReadToEnd();
                }
            }

            return resp;
        }
    }
}