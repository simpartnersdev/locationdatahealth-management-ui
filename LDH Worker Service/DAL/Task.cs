﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;

namespace LDH.Management.WorkerService.DAL
{
    public class Task
    {
        public List<Entities.Task> GetPendingTasks()
        {
            var result = new List<Entities.Task>();
            var conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["LDH_MySQL"].ConnectionString);
            var da = new MySqlDataAdapter("get_PendingTasks", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            var dt = new DataTable();

            try
            {
                conn.Open();
                da.Fill(dt);
                result = dt.ToList<Entities.Task>();
            }
            catch (Exception ex)
            {
                throw new TaskDataException("There was an error while retrieving Task data from the database.", ex);
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }

            return result;
        }

        public void SetQueuedTask(int TaskID)
        {
            var conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["LDH_MySQL"].ConnectionString);
            var cmd = new MySqlCommand("set_QueuedTask", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "task_id", DbType = DbType.Int32, Direction = ParameterDirection.Input, Value = TaskID });

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
        }

        public int CreateJob(int TaskID, int ProfileID, int SycaraBusinessID, string BusinessType)
        {
            var result = 0;
            var conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["LDH_MySQL"].ConnectionString);
            var cmd = new MySqlCommand("add_Job", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "tsk_id", DbType = DbType.Int32, Direction = ParameterDirection.Input, Value = TaskID });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "pro_id", DbType = DbType.Int32, Direction = ParameterDirection.Input, Value = ProfileID });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "sycara_business_listing_id", DbType = DbType.Int32, Direction = ParameterDirection.Input, Value = SycaraBusinessID });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "bus_type", DbType = DbType.String, Direction = ParameterDirection.Input, Value = BusinessType });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "job_id", DbType = DbType.Int32, Direction = ParameterDirection.Output });

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                result = (int)cmd.Parameters["job_id"].Value;
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }

            return result;
        }

        public int ActivateJob(int JobID, int ReportID, Entities.HydraJob ProfileData)
        {
            var result = 0;
            var conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["LDH_MySQL"].ConnectionString);
            var cmd = new MySqlCommand("add_ActiveJob", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "act_job_id", DbType = DbType.Int32, Direction = ParameterDirection.Input, Value = JobID });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "report_id", DbType = DbType.Int32, Direction = ParameterDirection.Input, Value = ReportID });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "pro_name", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProfileData.Name });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "pro_address", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProfileData.Address });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "pro_city", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProfileData.City });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "pro_state", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProfileData.State });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "pro_zip_code", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProfileData.ZipCode });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "pro_phone", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProfileData.Phone });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "pro_website", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProfileData.Website });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "pro_business_type", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProfileData.BusinessType });

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }

            return result;
        }
    }
}
