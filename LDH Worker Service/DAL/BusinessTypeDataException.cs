﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH.Management.WorkerService.DAL
{
    public class BusinessTypeDataException : Exception
    {
        public BusinessTypeDataException()
        {

        }

        public BusinessTypeDataException(Exception InnerException) : base(InnerException.Message, InnerException)
        {
        }

        public BusinessTypeDataException(string Message, Exception InnerException) : base(Message, InnerException)
        {

        }
    }
}