﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

using MySql.Data.MySqlClient;

using LDH.Management.WorkerService.Entities;
using LDH.Management.WorkerService.Enums;

namespace LDH.Management.WorkerService.DAL
{
    public class Directory
    {
        public List<Entities.Directory> GetDirectories(DirectoryStatus Status)
        {
            var result = new List<Entities.Directory>();
            var conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["LDH_MySQL"].ConnectionString);
            var da = new MySqlDataAdapter("get_Directories", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("directory_status", (int)Status);

            var dt = new DataTable();

            try
            {
                conn.Open();
                da.Fill(dt);
                result = dt.ToList<Entities.Directory>();
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }

            return result;
        }

        public List<HydraJob> GetProfileJobData(int DirectoryID)
        {
            var result = new List<HydraJob>();
            var conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["LDH_MySQL"].ConnectionString);
            var da = new MySqlDataAdapter("get_ProfileJobData", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("directory_id", DirectoryID);

            var dt = new DataTable();

            try
            {
                conn.Open();
                da.Fill(dt);
                result = dt.ToList<HydraJob>();
            }
            catch (Exception ex)
            {
                throw new DirectoryDataException("There was an error while retrieving Directory data from the database.", ex);
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }

            return result;
        }
    }
}