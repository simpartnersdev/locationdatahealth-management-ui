﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;

namespace LDH.Management.WorkerService.DAL
{
    public class Job
    {
        public List<Entities.Job> GetActiveJobs()
        {
            var result = new List<Entities.Job>();
            var conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["LDH_MySQL"].ConnectionString);
            var da = new MySqlDataAdapter("get_ActiveJobs", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            var dt = new DataTable();

            try
            {
                conn.Open();
                da.Fill(dt);
                result = dt.ToList<Entities.Job>();
            }
            catch (Exception ex)
            {
                throw new TaskDataException("There was an error while retrieving Active Jobs data from the database.", ex);
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }

            return result;
        }

        public void AddListingAccuracyData(Entities.AccuracyData ProcessedJobData)
        {
            var conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["LDH_MySQL"].ConnectionString);
            var cmd = new MySqlCommand("add_ListingAccuracyData", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "completed_job_id", DbType = DbType.Int32, Direction = ParameterDirection.Input, Value = ProcessedJobData.Job.ID });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "dom_id", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProcessedJobData.ParsedData.Domain });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "dom_weight", DbType = DbType.Decimal, Direction = ParameterDirection.Input, Value = ProcessedJobData.Domain.Weight });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "link", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProcessedJobData.ParsedData.Link });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "pro_name", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProcessedJobData.Job.Name });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "listing_name", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProcessedJobData.ParsedData.BusinessName });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "listing_name_match", DbType = DbType.Int16, Direction = ParameterDirection.Input, Value = (int)ProcessedJobData.NameMatch });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "pro_address", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProcessedJobData.Job.Address });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "listing_address", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProcessedJobData.ParsedData.Address });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "listing_address_match", DbType = DbType.Int16, Direction = ParameterDirection.Input, Value = (int)ProcessedJobData.AddressMatch });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "pro_city", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProcessedJobData.Job.City });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "listing_city", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProcessedJobData.ParsedData.City });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "listing_city_match", DbType = DbType.Int16, Direction = ParameterDirection.Input, Value = (int)ProcessedJobData.CityMatch });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "pro_state", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProcessedJobData.Job.State });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "listing_state", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProcessedJobData.ParsedData.State });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "listing_state_match", DbType = DbType.Int16, Direction = ParameterDirection.Input, Value = (int)ProcessedJobData.StateMatch });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "pro_zip_code", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProcessedJobData.Job.ZipCode });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "listing_zip_code", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProcessedJobData.ParsedData.ZipCode });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "listing_zip_code_match", DbType = DbType.Int16, Direction = ParameterDirection.Input, Value = ProcessedJobData.ZipCodeMatch });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "pro_phone", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProcessedJobData.Job.Phone });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "listing_phone", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProcessedJobData.ParsedData.Phone });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "listing_phone_match", DbType = DbType.Int16, Direction = ParameterDirection.Input, Value = ProcessedJobData.PhoneMatch });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "pro_website", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProcessedJobData.Job.Website });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "listing_website", DbType = DbType.String, Direction = ParameterDirection.Input, Value = ProcessedJobData.ParsedData.WebAddress });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "listing_website_match", DbType = DbType.Int16, Direction = ParameterDirection.Input, Value = ProcessedJobData.WebsiteMatch });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "listing_claimed", DbType = DbType.Boolean, Direction = ParameterDirection.Input, Value = ProcessedJobData.ListingClaimed });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "domain_score", DbType = DbType.Int32, Direction = ParameterDirection.Input, Value = ProcessedJobData.DomainScore });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "correctness_score", DbType = DbType.Decimal, Direction = ParameterDirection.Input, Value = ProcessedJobData.Correctness });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "health_score", DbType = DbType.Decimal, Direction = ParameterDirection.Input, Value = ProcessedJobData.HealthScore });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "name_weight", DbType = DbType.Decimal, Direction = ParameterDirection.Input, Value = ProcessedJobData.NameWeight });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "address_weight", DbType = DbType.Decimal, Direction = ParameterDirection.Input, Value = ProcessedJobData.AddressWeight });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "phone_weight", DbType = DbType.Decimal, Direction = ParameterDirection.Input, Value = ProcessedJobData.PhoneWeight });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "website_weight", DbType = DbType.Decimal, Direction = ParameterDirection.Input, Value = ProcessedJobData.WebsiteWeight });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "name_match", DbType = DbType.Boolean, Direction = ParameterDirection.Input, Value = ProcessedJobData.NameMatch });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "address_match", DbType = DbType.Boolean, Direction = ParameterDirection.Input, Value = ProcessedJobData.FullAddressMatch });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "phone_match", DbType = DbType.Boolean, Direction = ParameterDirection.Input, Value = ProcessedJobData.PhoneMatch });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "website_match", DbType = DbType.Boolean, Direction = ParameterDirection.Input, Value = ProcessedJobData.WebsiteMatch });

            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
        }

        public void CompleteActiveJob(int JobID, string JsonData)
        {
            var conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["LDH_MySQL"].ConnectionString);
            var cmd = new MySqlCommand("set_CompleteActiveJob", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "active_job_id", DbType = DbType.Int32, Direction = ParameterDirection.Input, Value = JobID });
            cmd.Parameters.Add(new MySqlParameter() { ParameterName = "json_data", DbType = DbType.String, Direction = ParameterDirection.Input, Value = JsonData });
           
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
        }
    }
}
