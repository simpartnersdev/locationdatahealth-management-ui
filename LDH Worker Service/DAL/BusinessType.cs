﻿using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace LDH.Management.WorkerService.DAL
{
    public static class BusinessType
    {
        private static List<Entities.BusinessType> BusinessTypes;

        public static Entities.BusinessType GetBusinessType(string BusinessType)
        {
            var returnValue = new Entities.BusinessType();

            if (BusinessTypes.Count == 0)
            {
                RefreshBusinessTypes();
            }
            
            returnValue = BusinessTypes.Find(x => x.Business.ToLower() == BusinessType.ToLower());
            if (returnValue == null)
            {
                returnValue = BusinessTypes.Find(x => x.Business.ToLower() == "default");
            }
            if (returnValue == null)
            {
                //last resort! local copy of default BT
                returnValue = GetDefault();
            }

            return returnValue;
        }

        public static void RefreshBusinessTypes()
        {
            var oLock = new object();
            //lock to avoid multiple simultaneous calls to the server, which may result in some random lockup situation
            lock (oLock)
            {
                var resp = string.Empty;
                var request = (HttpWebRequest)HttpWebRequest.Create(ConfigurationManager.AppSettings["BusinessTypesService"]);
                request.Headers.Add("Token", VelocityToken.Token);
                request.Method = "GET";

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.ContentEncoding == "gzip")
                    {
                        resp = Utils.gzUncompressText(response.GetResponseStream());
                    }
                    else
                    {
                        TextReader tr = new StreamReader(response.GetResponseStream());
                        resp = tr.ReadToEnd();
                    }
                }

                var BTServiceResponse = JsonConvert.DeserializeObject<Entities.BusinessTypesServiceResponse>(resp);
                BusinessTypes = new List<Entities.BusinessType>();
                var tmpBT = new Entities.BusinessType();

                foreach (var BT in BTServiceResponse.Settings)
                {
                    tmpBT = new Entities.BusinessType()
                    {
                        Business = BT.BusinessType,
                        Domains = JsonConvert.DeserializeObject<Entities.BusinessTypesDomains>(BT.Settings).Domains.FindAll(x => x.Weight > 0m)
                    };
                    //TODO: remove this nasty fix once the BT service fixes the names of the domains
                    for (int i = 0; i < tmpBT.Domains.Count; i++)
                    {
                        tmpBT.Domains[i].Name = tmpBT.Domains[i].Name.Replace(" ", string.Empty);
                    }

                    BusinessTypes.Add(tmpBT);
                }
            }
        }

        private static Entities.BusinessType GetDefault()
        {
            var defaultBT = new Entities.BusinessType()
            {
                Business = "default",
                Domains = new List<Entities.Domain>()
                {
                    new Entities.Domain() { Name = "BingLocal", Weight = 10m },
                    new Entities.Domain() { Name = "CitySearch", Weight = 3 },
                    new Entities.Domain() { Name = "ExpressUpdate", Weight = 4m },
                    new Entities.Domain() { Name = "Facebook", Weight = 10m },
                    new Entities.Domain() { Name = "Foursquare", Weight = 13m },
                    new Entities.Domain() { Name = "GoogleMyBusiness", Weight = 44m },
                    new Entities.Domain() { Name = "Local", Weight = 3m },
                    new Entities.Domain() { Name = "MapQuest", Weight = 1m },
                    new Entities.Domain() { Name = "MerchantCircle", Weight = 2m },
                    //new Entities.Domain() { Name = "TripAdvisor", Weight = 0m },
                    new Entities.Domain() { Name = "YahooLocal", Weight = 2m },
                    new Entities.Domain() { Name = "YellowPages", Weight = 3m },
                    new Entities.Domain() { Name = "Yelp", Weight = 5m }
                    //new Entities.Domain() { Name = "HealthGrades", Weight = 0m }
                    //new Entities.Domain() { Name = "MDcom", Weight = 0m }
                }
            };

            return defaultBT;
        }
    }
}