﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

namespace LDH.Management.WorkerService.DAL
{
    public static class Utils
    {
        private static Dictionary<string, string> AddressAbbreviations;
        private static string AddressAbbreviationsTimestamp;

        private static Dictionary<string, string> AddressUnitDesignators;
        private static string AddressUnitDesignatorsTimestamp;

        public static List<T> ToList<T>(this DataTable Table) where T : class, new()
        {
            string strColumn = string.Empty;
            try
            {
                List<T> list = new List<T>();

                T obj;
                PropertyInfo propertyInfo;

                foreach (var row in Table.AsEnumerable())
                {
                    obj = new T();

                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        if (prop.CustomAttributes.ToList().Exists(x => x.AttributeType.Name == "ColumnAttribute" &&
                                                                       x.NamedArguments.ToList().Exists(y => y.MemberName == "IsDbGenerated") &&
                                                                       x.NamedArguments.First(z => z.MemberName == "IsDbGenerated").TypedValue.Value.ToString().ToLower() == "false"))
                        {
                            continue;
                        }

                        try
                        {
                            if (prop.CustomAttributes.ToList().Exists(x => x.AttributeType.Name == "ColumnAttribute" && x.NamedArguments.ToList().Exists(y => y.MemberName == "Name")))
                            {
                                strColumn = prop.GetCustomAttributesData().First(x => x.AttributeType.Name == "ColumnAttribute").NamedArguments.First(y => y.MemberName == "Name").TypedValue.Value.ToString();
                            }
                            else
                            {
                                strColumn = prop.Name;
                            }

                            propertyInfo = obj.GetType().GetProperty(prop.Name);
                            if (prop.PropertyType.Equals(typeof(bool)) && prop.CustomAttributes.ToList().Exists(x => x.AttributeType.Name == "ColumnAttribute" &&
                                                                                                                     x.NamedArguments.ToList().Exists(y => y.MemberName == "DbType") &&
                                                                                                                     x.NamedArguments.First(z => z.MemberName == "DbType").TypedValue.Value.ToString().ToLower() == "varchar(1)"))
                            {
                                propertyInfo.SetValue(obj, row[strColumn].ToString().ToUpper() == "Y" || row[strColumn].ToString().ToUpper() == "T" ? true : false, null);
                            }
                            else if (prop.PropertyType.IsEnum)
                            {
                                propertyInfo.SetValue(obj, Enum.Parse(Type.GetType(propertyInfo.PropertyType.FullName), row[strColumn].ToString(), true));
                            }
                            else
                            {
                                propertyInfo.SetValue(obj, Convert.ChangeType(row[strColumn], propertyInfo.PropertyType), null);
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }

                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }

        public static string GetRedshiftConnectionString()
        {
            //determine driver version
            string driverVersion = "x86";
            if (IntPtr.Size == 8)
                driverVersion = "x64";

            return string.Format(ConfigurationManager.ConnectionStrings["RedshiftConnection"].ConnectionString, driverVersion);
        }

        public static string ToCsv(this List<string> List)
        {
            StringBuilder sbCSV = new StringBuilder();
            foreach (var value in List)
            {
                sbCSV.Append(string.Format(",'{0}'", value));
            }

            if (sbCSV.Length == 0)
            {
                return string.Empty;
            }

            return sbCSV.ToString().Substring(1);
        }

        public static string ToCsv(this List<int> List)
        {
            StringBuilder sbCSV = new StringBuilder();
            foreach (var value in List)
            {
                sbCSV.Append(string.Format(",{0}", value));
            }

            if (sbCSV.Length == 0)
            {
                return string.Empty;
            }

            return sbCSV.ToString().Substring(1);
        }

        public static string gzUncompressText(Stream data)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                data.CopyTo(ms);

                using (var mem = new MemoryStream())
                {
                    //write magic number (it's called like that) in the header of the stream
                    mem.Write(new byte[] { 0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00 }, 0, 8);
                    mem.Write(ms.ToArray(), 0, ms.ToArray().Length);

                    mem.Position = 0;

                    //now uncompress the thing
                    using (var gzip = new GZipStream(mem, CompressionMode.Decompress))
                    using (var reader = new StreamReader(gzip))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }

        ///*****************************
        /// Compute Levenshtein distance 
        /// Memory efficient version
        ///*****************************
        public static int LevenshteinDistance(this string StringA, string StringB)
        {
            
            int RowLen = StringA.Length;  // length of sRow
            int ColLen = StringB.Length;  // length of sCol
            int RowIdx;                // iterates through sRow
            int ColIdx;                // iterates through sCol
            char Row_i;                // ith character of sRow
            char Col_j;                // jth character of sCol
            int cost;                   // cost

            /// Test string length
            if (Math.Max(StringA.Length, StringB.Length) > Math.Pow(2, 31))
                throw (new Exception("\nMaximum string length for LevenshteinDistance is " + Math.Pow(2, 31) + ".\nYours is " + Math.Max(StringA.Length, StringB.Length) + "."));

            // Step 1

            if (RowLen == 0)
            {
                return ColLen;
            }

            if (ColLen == 0)
            {
                return RowLen;
            }

            /// Create the two vectors
            int[] v0 = new int[RowLen + 1];
            int[] v1 = new int[RowLen + 1];
            int[] vTmp;



            /// Step 2
            /// Initialize the first vector
            for (RowIdx = 1; RowIdx <= RowLen; RowIdx++)
            {
                v0[RowIdx] = RowIdx;
            }

            // Step 3

            /// Fore each column
            for (ColIdx = 1; ColIdx <= ColLen; ColIdx++)
            {
                /// Set the 0'th element to the column number
                v1[0] = ColIdx;

                Col_j = StringB[ColIdx - 1];


                // Step 4

                /// Fore each row
                for (RowIdx = 1; RowIdx <= RowLen; RowIdx++)
                {
                    Row_i = StringA[RowIdx - 1];


                    // Step 5

                    if (Row_i == Col_j)
                    {
                        cost = 0;
                    }
                    else
                    {
                        cost = 1;
                    }

                    // Step 6

                    /// Find minimum
                    int m_min = v0[RowIdx] + 1;
                    int b = v1[RowIdx - 1] + 1;
                    int c = v0[RowIdx - 1] + cost;

                    if (b < m_min)
                    {
                        m_min = b;
                    }
                    if (c < m_min)
                    {
                        m_min = c;
                    }

                    v1[RowIdx] = m_min;
                }

                /// Swap the vectors
                vTmp = v0;
                v0 = v1;
                v1 = vTmp;

            }


            // Step 7

            /// Value between 0 - 100
            /// 0==perfect match 100==totaly different
            /// 
            /// The vectors where swaped one last time at the end of the last loop,
            /// that is why the result is now in v0 rather than in v1
            //System.Console.WriteLine("iDist=" + v0[RowLen]);
            //int max = System.Math.Max(RowLen, ColLen);
            //return ((100 * v0[RowLen]) / max);

            //for LDH operation we need the int value of the distance
            return v0[RowLen];
        }

        public static string FormatURLStandard(string URL)
        {
            var pos = URL.IndexOf("://");
            if (pos < 0 || pos > 5 || URL == string.Empty)
                return "http://" + URL;
            else
                return URL;
        }

        public static string GetAddressAbreviation(string AddressPart)
        {
            var AbbreviationsFile = new FileInfo(ConfigurationManager.AppSettings["AddressAbbreviationsFile"]);
            if (AddressAbbreviations == null || AddressAbbreviationsTimestamp == null || AddressAbbreviationsTimestamp != AbbreviationsFile.LastWriteTime.ToString("o"))
            {
                var lockobj = new object();
                lock (lockobj)
                {
                    //standard lock double-check
                    if (AddressAbbreviations == null || AddressAbbreviationsTimestamp == null || AddressAbbreviationsTimestamp != AbbreviationsFile.LastWriteTime.ToString("o"))
                    {
                        RefreshAbbreviations();
                    }
                }
            }

            if (AddressAbbreviations.ContainsKey(AddressPart))
            {
                return AddressAbbreviations[AddressPart];
            }
            else
            {
                return AddressPart;
            }
        }

        private static void RefreshAbbreviations()
        {
            try
            {
                var BasePath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                using (var AbbreviationsFile = File.OpenText(BasePath + @"\" + ConfigurationManager.AppSettings["AddressAbbreviationsFile"]))
                {
                    var line = string.Empty;
                    var word = string.Empty;
                    var abbr = string.Empty;
                    AddressAbbreviations = new Dictionary<string, string>();

                    while ((line = AbbreviationsFile.ReadLine()) != null)
                    {
                        word = line.Split(',')[0];
                        abbr = line.Split(',')[1];

                        //this will have the effect of not causing an exception for having a duped definition,
                        //and also means that the last definition will be in effect
                        if (AddressAbbreviations.ContainsKey(word))
                        {
                            AddressAbbreviations[word] = abbr;
                        }
                        else
                        {
                            AddressAbbreviations.Add(word, abbr);
                        }
                    }
                    AddressAbbreviationsTimestamp = (new FileInfo(ConfigurationManager.AppSettings["AddressAbbreviationsFile"])).LastWriteTime.ToString("o");
                }
            }
            catch (Exception ex)
            {
                DAL.Log.AddMessage(string.Format("There was an error while reading abbreviations from the file: {0}\r\nrror: {1}\r\nStack Trace: {2}", ConfigurationManager.AppSettings["AddressAbbreviationsFile"], ex.Message, ex.StackTrace));
            }
        }

        public static string GetAddressUnitDesignatorAbreviation(string UnitDesignator)
        {
            var AbbreviationsFile = new FileInfo(ConfigurationManager.AppSettings["AddressUnitDesignatorsFile"]);
            if (AddressUnitDesignators == null || AddressUnitDesignatorsTimestamp == null || AddressUnitDesignatorsTimestamp != AbbreviationsFile.LastWriteTime.ToString("o"))
            {
                var lockobj = new object();
                lock (lockobj)
                {
                    //standard lock double-check
                    if (AddressUnitDesignators == null || AddressUnitDesignatorsTimestamp == null || AddressUnitDesignatorsTimestamp != AbbreviationsFile.LastWriteTime.ToString("o"))
                    {
                        RefreshUnitDesignator();
                    }
                }
            }

            if (AddressUnitDesignators.ContainsKey(UnitDesignator))
            {
                return AddressUnitDesignators[UnitDesignator];
            }
            else
            {
                return UnitDesignator;
            }
        }

        private static void RefreshUnitDesignator()
        {
            try
            {
                var BasePath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                using (var AbbreviationsFile = File.OpenText(BasePath + @"\" + ConfigurationManager.AppSettings["AddressUnitDesignatorsFile"]))
                {
                    var line = string.Empty;
                    var word = string.Empty;
                    var abbr = string.Empty;
                    AddressUnitDesignators = new Dictionary<string, string>();

                    while ((line = AbbreviationsFile.ReadLine()) != null)
                    {
                        word = line.Split(',')[0];
                        abbr = line.Split(',')[1];

                        //this will have the effect of not causing an exception for having a duped definition,
                        //and also means that the last definition will be in effect
                        if (AddressUnitDesignators.ContainsKey(word))
                        {
                            AddressUnitDesignators[word] = abbr;
                        }
                        else
                        {
                            AddressUnitDesignators.Add(word, abbr);
                        }
                    }
                    AddressUnitDesignatorsTimestamp = (new FileInfo(ConfigurationManager.AppSettings["AddressUnitDesignatorsFile"])).LastWriteTime.ToString("o");
                }
            }
            catch (Exception ex)
            {
                DAL.Log.AddMessage(string.Format("There was an error while reading unit designator abbreviations from the file: {0}\r\nrror: {1}\r\nStack Trace: {2}", ConfigurationManager.AppSettings["AddressUnitDesignatorsFile"], ex.Message, ex.StackTrace));
            }
        }
    }
}