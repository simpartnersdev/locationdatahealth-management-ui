﻿namespace LDH.Management.WorkerService.Enums
{
    public enum MatchLevel
    {
        //Match values taken from original scale in Sycara
        NoMatch = 0,
        Partial = 2,
        Complete = 3
    }
}
