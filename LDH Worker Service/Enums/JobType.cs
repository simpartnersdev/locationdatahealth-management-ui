﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace LDH.Management.WebUI.Models.Enums
{
    public enum JobType
    {
        [Description(@"jobs\\DirectoryListing")]
        DirectoryListing,
        [Description(@"jobs\\ListingReviews")]
        ListingReviews,
        [Description(@"jobs\\MapRanking")]
        MapRanking,
        [Description(@"jobs\\MobileRanking")]
        MobileRanking,
        [Description(@"jobs\\OrganicRanking")]
        OrganicRanking
    }
}