﻿using System.Data.Linq.Mapping;

namespace LDH.Management.WorkerService.Entities
{
    public class Task
    {
        [Column(Name = "tsk_id")]
        public int ID { get; set; }
        [Column(Name = "tsk_prc_id")]
        public int ProcessID { get; set; }
        [Column(Name = "tsk_dir_id")]
        public int DirectoryID { get; set; }
        [Column(Name = "tsk_sycara_account_id")]
        public int SycaraAccountID { get; set; }
    }
}
