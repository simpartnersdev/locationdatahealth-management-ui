﻿using System.Data.Linq.Mapping;

namespace LDH.Management.WorkerService.Entities
{
    public class Job
    {
        [Column(Name = "job_id")]
        public int ID { get; set; }

        [Column(Name = "job_pro_id")]
        public int ProfileID { get; set; }

        [Column(Name = "job_report_id")]
        public int ReportID { get; set; }

        [Column(Name = "job_sycara_business_listing_id")]
        public int SycaraID { get; set; }

        [Column(Name = "job_tsk_id")]
        public int TaskID { get; set; }

        [Column(Name = "api_key")]
        public string ApiKey { get; set; }

        [Column(Name = "job_pro_name")]
        public string Name { get; set; }

        [Column(Name = "job_pro_address")]
        public string Address { get; set; }

        [Column(Name = "job_pro_city")]
        public string City { get; set; }

        [Column(Name = "job_pro_state")]
        public string State { get; set; }

        [Column(Name = "job_pro_zip_code")]
        public string ZipCode { get; set; }

        [Column(Name = "job_pro_phone")]
        public string Phone { get; set; }

        [Column(Name = "job_pro_website")]
        public string Website { get; set; }

        [Column(Name = "job_pro_business_type")]
        public string BusinessType { get; set; }

        [Column(Name = "velocity_directory_business_type")]
        public string DirectoryBusinessType { get; set; }
    }
}
