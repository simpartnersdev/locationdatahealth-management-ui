﻿using System.Data.Linq.Mapping;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace LDH.Management.WorkerService.Entities
{
    public class HydraJob
    {
        [Column(Name = "api_key")]
        [JsonIgnore]
        [ScriptIgnore]
        public string ApiKey { get; set; }

        [Column(Name = "velocity_profile_id")]
        [JsonProperty(PropertyName = "id")]
        public int ID { get; set; }

        [Column(Name = "business_listing_name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [Column(Name = "business_listing_address")]
        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }

        [Column(Name = "business_listing_city")]
        [JsonProperty(PropertyName = "city")]
        public string City { get; set; }

        [Column(Name = "business_listing_state")]
        [JsonProperty(PropertyName = "state")]
        public string State { get; set; }

        [Column(Name = "business_listing_zip")]
        [JsonProperty(PropertyName = "zip_code")]
        public string ZipCode { get; set; }

        [Column(Name = "business_listing_country")]
        [JsonProperty(PropertyName = "country")]
        public string Country { get; set; }

        [Column(Name = "business_listing_phone_number")]
        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }

        [Column(Name = "business_listing_website_url")]
        [JsonIgnore]
        [ScriptIgnore]
        public string Website { get; set; }

        [Column(Name = "engine")]
        [JsonProperty(PropertyName = "engine")]
        public string Engine { get; set; }

        [Column(Name = "language")]
        [JsonProperty(PropertyName = "language")]
        public string Language { get; set; }

        [Column(Name = "business_listing_id")]
        [JsonIgnore]
        [ScriptIgnore]
        public int SycaraBusinessID { get; set; }

        [Column(Name = "velocity_directory_id")]
        [JsonIgnore]
        [ScriptIgnore]
        public int DirectoryID { get; set; }

        [Column(Name = "account_id")]
        [JsonIgnore]
        [ScriptIgnore]
        public int SycaraAccountID { get; set; }

        [Column(Name = "velocity_business_type")]
        [JsonIgnore]
        [ScriptIgnore]
        public string BusinessType { get; set; }

        [Column(Name = "velocity_directory_business_type")]
        [JsonIgnore]
        [ScriptIgnore]
        public string DirectoryBusinessType { get; set; }
    }
}