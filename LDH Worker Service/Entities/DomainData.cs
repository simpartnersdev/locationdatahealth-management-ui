﻿using Newtonsoft.Json;

namespace LDH.Management.WorkerService.Entities
{
    public class DomainData
    {
        public string ID { get; set; }

        [JsonProperty(PropertyName = "business_listing_id")]
        public int SycaraBusinessID { get; set; }

        [JsonProperty(PropertyName = "engine")]
        public string Domain { get; set; }

        public int Date { get; set; }

        [JsonProperty(PropertyName = "external_id")]
        public string ExternalID { get; set; }

        public string Link { get; set; }

        public string Claimed { get; set; }

        [JsonProperty(PropertyName = "business_name")]
        public string BusinessName { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        [JsonProperty(PropertyName = "postal_code")]
        public string ZipCode { get; set; }

        public string Phone { get; set; }

        [JsonProperty(PropertyName = "web_address")]
        public string WebAddress { get; set; }
    }
}
