﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDH.Management.WorkerService.Entities
{
    public class CSVExportSettings
    {
        public DateTime ExportDate { get; set; }
        public List<CSVDirectoryProcess> Directories { get; set; }
    }

    public class CSVDirectoryProcess
    {
        public int DirectoryID { get; set; }
        public int ProcessID { get; set; }
    }
}
