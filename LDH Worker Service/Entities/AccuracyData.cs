﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;

using LDH.Management.WorkerService.Enums;
using LDH.Management.WorkerService.DAL;


namespace LDH.Management.WorkerService.Entities
{
    public class AccuracyData
    {
        public MatchLevel NameMatch { get; private set; }
        public MatchLevel AddressMatch { get; private set; }
        public MatchLevel CityMatch { get; private set; }
        public MatchLevel StateMatch { get; private set; }
        public MatchLevel ZipCodeMatch { get; private set; }
        public MatchLevel PhoneMatch { get; private set; }
        public MatchLevel WebsiteMatch { get; private set; }

        public MatchLevel FullAddressMatch
        {
            get
            {
                if (AddressMatch == MatchLevel.Complete && CityMatch == MatchLevel.Complete && StateMatch == MatchLevel.Complete && ZipCodeMatch == MatchLevel.Complete)
                {
                    return MatchLevel.Complete;
                }
                else if (AddressMatch == MatchLevel.NoMatch || CityMatch == MatchLevel.NoMatch || StateMatch == MatchLevel.NoMatch || ZipCodeMatch == MatchLevel.NoMatch)
                {
                    return MatchLevel.NoMatch;
                }
                else
                {
                    return MatchLevel.Partial;
                }
            }
        }
        public int DomainScore { get; private set; }
        public decimal Correctness { get; private set; }
        public decimal HealthScore { get; private set; }
        public bool ListingClaimed
        {
            get
            {
                var val = false;
                switch (ParsedData.Claimed.ToLower().Trim())
                {
                    case "true":
                    case "1":
                    case "yes":
                    case "y":
                    case "t":
                        val = true;
                        break;
                }
                return val;
            }
        }

        public decimal NameWeight { get; private set; }
        public decimal AddressWeight { get; private set; }
        public decimal PhoneWeight { get; private set; }
        public decimal WebsiteWeight { get; private set; }

        private decimal NameScoreThreshold { get; set; }
        private int PartialMatchThreshold { get; set; }

        public Job Job { get; private set; }
        public DomainData ParsedData { get; private set; }
        public Domain Domain { get; private set; }

        public AccuracyData(Entities.Job Job, Entities.DomainData ParsedData)
        {
            //load the instance with the constructor parameters 
            this.Job = Job;
            this.ParsedData = ParsedData;
            this.Domain = DAL.BusinessType.GetBusinessType(string.IsNullOrEmpty(Job.BusinessType) ? Job.DirectoryBusinessType : Job.BusinessType).Domains.Find(x => x.Name.ToLower() == ParsedData.Domain.ToLower());

            if (this.Domain == null)
            {
                //Domain doesn't exist in the list, that means it has weight = 0
                this.Domain = new Domain() { Name = ParsedData.Domain, Weight = 0m };
            }

            //now get the settings from .config file
            NameWeight = decimal.Parse(ConfigurationManager.AppSettings["NameWeight"].Replace(',', '.'), CultureInfo.InvariantCulture);
            AddressWeight = decimal.Parse(ConfigurationManager.AppSettings["AddressWeight"].Replace(',', '.'), CultureInfo.InvariantCulture);
            PhoneWeight = decimal.Parse(ConfigurationManager.AppSettings["PhoneWeight"].Replace(',', '.'), CultureInfo.InvariantCulture);
            WebsiteWeight = decimal.Parse(ConfigurationManager.AppSettings["WebsiteWeight"].Replace(',', '.'), CultureInfo.InvariantCulture);
            NameScoreThreshold = decimal.Parse(ConfigurationManager.AppSettings["NameScoreThreshold"].Replace(',', '.'), CultureInfo.InvariantCulture);
            PartialMatchThreshold = int.Parse(ConfigurationManager.AppSettings["PartialMatchThreshold"], CultureInfo.InvariantCulture);
            
            CalculateCorrectness();
            CalculateDomainScore();
            CalculateHealthScore();
        }

        private void CalculateDomainScore()
        {
            var MatchValues = new List<int>();
            MatchValues.Add((int)NameMatch);
            MatchValues.Add((int)AddressMatch);
            MatchValues.Add((int)CityMatch);
            MatchValues.Add((int)StateMatch);
            MatchValues.Add((int)ZipCodeMatch);
            MatchValues.Add((int)PhoneMatch);
            MatchValues.Add((int)WebsiteMatch);

            DomainScore = (MatchValues.Sum() * 100) / (MatchValues.Count * (int)MatchLevel.Complete);
        }

        private void CalculateCorrectness()
        {
            NameMatch = GetNameMatch();
            AddressMatch = GetAddressMatch();
            CityMatch = GetCityMatch();
            StateMatch = GetStateMatch();
            ZipCodeMatch = GetZipCodeMatch();
            PhoneMatch = GetPhoneMatch();
            WebsiteMatch = GetWebsiteMatch();
            
            Correctness = 0m;
            Correctness += NameMatch == MatchLevel.Complete ? NameWeight : 0m;
            Correctness += AddressMatch == MatchLevel.Complete && CityMatch == MatchLevel.Complete && StateMatch == MatchLevel.Complete && ZipCodeMatch == MatchLevel.Complete ? AddressWeight : 0m;
            Correctness += PhoneMatch == MatchLevel.Complete ? PhoneWeight : 0m;
            Correctness += WebsiteMatch == MatchLevel.Complete ? WebsiteWeight : 0m;
        }

        private void CalculateHealthScore()
        {
            HealthScore = Correctness * Domain.Weight;
        }

        private MatchLevel GetNameMatch()
        {
            var returnValue = MatchLevel.NoMatch;
            //HTML-decode the text
            var ProfileName = WebUtility.HtmlDecode(Job.Name);
            var ListingName = WebUtility.HtmlDecode(ParsedData.BusinessName);

            //now clean it
            ProfileName = CleanString(ProfileName);
            ListingName = CleanString(ListingName);

            //compare it
            if (CompareStrings(ProfileName, ListingName) >= NameScoreThreshold)
            {
                returnValue = MatchLevel.Complete;
            }

            return returnValue;
        }

        private MatchLevel GetAddressMatch()
        {
            var ProfileAddress = FormatAddress(Job.Address);
            var ListingAddress = FormatAddress(ParsedData.Address);

            FixAddressUnitDesignators(ref ProfileAddress, ref ListingAddress);

            return CalculateMatch(ProfileAddress, ListingAddress);
        }

        private string FormatAddress(string Address)
        {
            //uppercase the whole string
            var returnValue = Address.ToUpper();

            //remove unnecessary parts
            returnValue = returnValue.Replace(",", " ");
            returnValue = returnValue.Replace(".", " ");

            //fix PO BOXes
            returnValue = returnValue.Replace("P O BOX", "PO BOX");
            returnValue = returnValue.Replace("P O  BOX", "PO BOX");

            //put a space between # and the surrounding address parts
            returnValue = returnValue.Replace("#", " # ");

            //split into a list of address parts and remove empty parts
            var parts = returnValue.Split(' ').Select(x => x.Trim()).Where(x => !string.IsNullOrEmpty(x)).ToList<string>();

            //standardize abbreviations
            for (int i = 0; i < parts.Count; i++)
            {
                parts[i] = DAL.Utils.GetAddressAbreviation(parts[i]);
            }

            //rebuild the address
            returnValue = string.Join(" ", parts);

            return returnValue;
        }

        private void FixAddressUnitDesignators(ref string Address1, ref string Address2)
        {
            var AddressParts1 = Address1.Split(' ');
            var AddressParts2 = Address2.Split(' ');

            //convert unit designator to accepted abbreviation ONLY IF the other address matches the same unit designator abbreviation (typically #)
            for (int i = 0; i < AddressParts1.Length && i < AddressParts2.Length; i++)
            {
                if (DAL.Utils.GetAddressUnitDesignatorAbreviation(AddressParts1[i]) == AddressParts2[i])
                {
                    AddressParts1[i] = DAL.Utils.GetAddressUnitDesignatorAbreviation(AddressParts1[i]);
                }

                if (DAL.Utils.GetAddressUnitDesignatorAbreviation(AddressParts2[i]) == AddressParts1[i])
                {
                    AddressParts2[i] = DAL.Utils.GetAddressUnitDesignatorAbreviation(AddressParts2[i]);
                }
            }

            //rebuild the address
            Address1 = string.Join(" ", AddressParts1);
            Address2 = string.Join(" ", AddressParts2);
        }

        private MatchLevel GetCityMatch()
        {
            return CalculateMatch(Job.City, ParsedData.City);
        }

        private MatchLevel GetStateMatch()
        {
            var ProfileState = CleanString(Job.State);
            var ListingState = CleanString(ParsedData.State);
            var returnValue = MatchLevel.NoMatch;

            if (ProfileState == ListingState)
            {
                returnValue = MatchLevel.Complete;
            }
            else
            {
                if (DAL.State.GetState(ProfileState) == DAL.State.GetState(ListingState))
                {
                    returnValue = MatchLevel.Complete;
                }
            }

            return returnValue;
        }

        private MatchLevel GetZipCodeMatch()
        {
            var ProfileZipCode = Job.ZipCode.Trim();
            var ListingZipCode = ParsedData.ZipCode.Trim();
            var returnValue = MatchLevel.NoMatch;

            if (ProfileZipCode == ListingZipCode)
            {
                returnValue = MatchLevel.Complete;
            }

            return returnValue;
        }

        private MatchLevel GetPhoneMatch()
        {
            var ProfilePhone = Regex.Replace(Job.Phone, @"\D", "", RegexOptions.Compiled);
            var ListingPhone = Regex.Replace(ParsedData.Phone, @"\D", "", RegexOptions.Compiled);
            var returnValue = MatchLevel.NoMatch;

            if (ProfilePhone == ListingPhone)
            {
                returnValue = MatchLevel.Complete;
            }

            return returnValue;
        }

        private MatchLevel GetWebsiteMatch()
        {
            var ProfileWebsite = Job.Website.Trim().ToLower().Replace("www.", string.Empty).Replace("https://", "http://");
            var ListingWebsite = ParsedData.WebAddress.Trim().ToLower().Replace("www.", string.Empty).Replace("https://", "http://");

            //remove GMB campaign URL Parameter
            ProfileWebsite = Regex.Replace(ProfileWebsite, ConfigurationManager.AppSettings["REMOVE_URL_Parameter"], string.Empty);
            ListingWebsite = Regex.Replace(ListingWebsite, ConfigurationManager.AppSettings["REMOVE_URL_Parameter"], string.Empty);

            var returnValue = MatchLevel.NoMatch;

            if (ProfileWebsite == ListingWebsite)
            {
                returnValue = MatchLevel.Complete;
            }
            else if (ListingWebsite == string.Empty || ProfileWebsite == string.Empty)
            {
                returnValue = MatchLevel.NoMatch;
            }
            else if (Regex.Replace(ProfileWebsite, @"[^a-z0-9\-]", "", RegexOptions.Compiled) == Regex.Replace(ListingWebsite, @"[^a-z0-9\-]", "", RegexOptions.Compiled))
            {
                returnValue = MatchLevel.Complete;
            }
            else
            {
                if (!Regex.IsMatch(ProfileWebsite, "https?://", RegexOptions.Compiled))
                {
                    ProfileWebsite = "http://" + ProfileWebsite;
                }
                if (!Regex.IsMatch(ListingWebsite, "https?://", RegexOptions.Compiled))
                {
                    ListingWebsite = "http://" + ListingWebsite;
                }

                try
                {
                    var ProfileURI = new Uri(ProfileWebsite);
                    var ListingURI = new Uri(ListingWebsite);

                    if (ProfileURI.Host.IndexOf(ListingURI.Host) >= 0 || ListingURI.Host.IndexOf(ProfileURI.Host) >= 0)
                    {
                        //one contains the other, we let it have a partial match
                        returnValue = MatchLevel.Partial;
                    }
                }
                catch
                {
                    //malformed URI, so must be a no-match
                    returnValue = MatchLevel.NoMatch;
                }
            }

            return returnValue;
        }

        private MatchLevel CalculateMatch(string TextA, string TextB)
        {
            if (TextA == null)
                throw new System.ArgumentNullException("CalculateMatch: string parameter TextA must not be null.");
            if (TextB == null)
                throw new System.ArgumentNullException("CalculateMatch: string parameter TextB must not be null.");

            var returnValue = MatchLevel.NoMatch;

            TextA = CleanString(TextA);
            TextB = CleanString(TextB);

            if (TextA == TextB)
            {
                returnValue = MatchLevel.Complete;
            }
            else if (TextA.LevenshteinDistance(TextB) < PartialMatchThreshold)
            {
                returnValue = MatchLevel.Partial;
            }
            
            return returnValue;
        }

        private string CleanString(string Text)
        {
            if (Text == null)
                throw new System.ArgumentNullException("CleanString: string parameter Text must not be null.");

            var returnValue = Text.Trim().ToLower();
            returnValue = returnValue.Replace(",", string.Empty).Replace(".", string.Empty);

            return returnValue;
        }

        private decimal CompareStrings(string TextA, string TextB)
        {
            if (TextA == null)
                throw new System.ArgumentNullException("CompareStrings: string parameter TextA must not be null.");
            if (TextB == null)
                throw new System.ArgumentNullException("CompareStrings: string parameter TextB must not be null.");

            var pairs1 = WordLetterPairs(TextA.ToLower());
            var pairs2 = WordLetterPairs(TextB.ToLower());

            var union = pairs1.Count + pairs2.Count;
            var intersection = 0;

            for (int i = 0; i < pairs1.Count; i++)
            {
                for (int j = 0; j < pairs2.Count; j++)
                {
                    if (pairs1[i].Equals(pairs2[j]))
                    {
                        intersection = pairs1.Intersect(pairs2).Count();
                    }
                }
            }

            if (union == 0)
            {
                return 0;
            }

            return (2.0m * intersection) / union;
        }

        private List<string> WordLetterPairs(string Text)
        {
            var allPairs = new List<string>();
            var pairsInWord = new List<string>();
            var words = Regex.Split(Text, @"\s", RegexOptions.Compiled).ToList();

            for (int i = 0; i < words.Count; i++)
            {
                allPairs.AddRange(LetterPairs(words[i]));
            }

            return allPairs;
        }

        private List<string> LetterPairs(string Text)
        {
            List<string> pairs = new List<string>();
            for (int i = 0; i < Text.Length - 1; i++)
            {
                pairs.Add(Text.Substring(i, 2));
            }
            return pairs;
        }
    }
}
