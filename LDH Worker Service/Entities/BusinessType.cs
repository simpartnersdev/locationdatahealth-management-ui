﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDH.Management.WorkerService.Entities
{
    public class BusinessType
    {
        public string Business { get; set; }
        public List<Domain> Domains { get; set; }
    }
}
