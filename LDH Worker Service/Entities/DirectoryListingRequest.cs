﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace LDH.Management.WorkerService.Entities
{
    public class DirectoryListingRequest
    {
        [JsonProperty(PropertyName = "api_key")]
        public string Apikey { get; set; }
        [JsonProperty(PropertyName = "listing_requests")]
        public List<HydraJob> ListingRequests { get; set; }
    }
}