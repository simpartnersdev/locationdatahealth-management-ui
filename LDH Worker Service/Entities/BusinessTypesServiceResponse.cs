﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDH.Management.WorkerService.Entities
{
    public class BusinessTypesServiceResponse
    {
        public List<BusinessTypesSettings> Settings { get; set; }
    }

    public class BusinessTypesSettings
    {
        public string BusinessType { get; set; }
        public string Settings { get; set; }
    }

    public class BusinessTypesDomains
    {
        public List<Domain> Domains;
    }
}
