﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDH.Management.WorkerService.Entities
{
    public class Domain
    {
        public string Name { get; set; }
        public decimal Weight { get; set; }
    }
}
