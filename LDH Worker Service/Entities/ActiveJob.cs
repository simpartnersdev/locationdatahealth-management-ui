﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LDH.Management.WorkerService.Entities
{
    public class ActiveJob
    {
        public int ID { get; set; }
        public Directory Account { get; set; }
        public int MyProperty { get; set; }
    }
}