﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace LDH.Management.WorkerService
{
    public partial class LDHWorkerService : ServiceBase
    {
        private Timer seeder = null;
        private Timer harvester = null;
        private BL.Task seed_worker = null;
        private BL.Job harvest_worker = null;

        public LDHWorkerService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var seedInterval = Convert.ToDouble(ConfigurationManager.AppSettings["SeedInterval"]);
            seed_worker = new BL.Task();

            seeder = new Timer(seedInterval);
            seeder.AutoReset = false;
            seeder.Elapsed += new ElapsedEventHandler(seed_worker.ProcessPendingTasks);
            seeder.Start();

            var harvestInterval = Convert.ToDouble(ConfigurationManager.AppSettings["HarvestInterval"]);
            harvest_worker = new BL.Job();

            harvester = new Timer(harvestInterval);
            harvester.AutoReset = false;
            harvester.Elapsed += new ElapsedEventHandler(harvest_worker.CollectCompletedJobs);
            harvester.Start();

            DAL.Log.AddMessage("LDH Worker Service started.");
        }

        protected override void OnStop()
        {
            seed_worker = null;
            harvest_worker = null;
            seeder.Enabled = false;
            harvester.Enabled = false;
            DAL.Log.AddMessage("LDH Worker Service stopped.");
        }
    }
}
